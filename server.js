require('dotenv').config();
var echo = require('laravel-echo-server');

var options = {
    authPath: '/broadcasting/auth',
    authHost: process.env.APP_URL,
    host: process.env.APP_URL,
};

echo.run(options);
