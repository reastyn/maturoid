<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class UserNotification extends Notification
{
    use Queueable;

    /**
     * @var
     */
    protected $title;
    /**
     * @var
     */
    protected $body;
    /**
     * @var
     */
    protected $style;

    /**
     * @var
     */
    protected $icon;

    /**
     * @var
     */
    protected $url;

    /**
     * Create a new notification instance.
     */
    public function __construct($props)
    {
        foreach ($props as $key => $value) {
            $this->{$key} = $value;
        }
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array
     */
    public function via()
    {
        return ['broadcast'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'body' => $this->body,
            'title' => $this->title,
            'style' => $this->style,
            'icon' => $this->icon,
            'url' => $this->url
        ];
    }
}
