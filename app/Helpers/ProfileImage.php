<?php
namespace App\Helpers;

use Illuminate\Support\Str;
use Image;
use Log;
use Storage;

class ProfileImage
{
    const SMALL = 100;
    const MEDIUM = 500;
    const LARGE = 1500;

    /**
     * @param string $name
     * @return string
     */
    public function generate($name = '77') {
        $str = $this->formatLettersFromName($name);
        $color = Utils::$colors[array_rand(Utils::$colors)];
        $folder = md5(microtime());

        Storage::disk('profile')->makeDirectory($folder);

        $this->generateImage($str, $color, self::SMALL, $folder);
        $this->generateImage($str, $color, self::MEDIUM, $folder);
        $this->generateImage($str, $color, self::LARGE, $folder);

        return $folder;
    }

    /**
     * @param $str
     * @param $color
     * @param $size
     * @param $folder
     */
    private function generateImage($str, $color, $size, $folder) {
        $img = Image::canvas($size, $size, $color)->text($str, $size / 2, $size / 2, function($font) use($size) {
            $font->file(public_path() . '/css/fonts/Roboto-Bold.ttf');
            $font->size($size / 2);
            $font->color('#ffffff');
            $font->align('center');
            $font->valign('middle');
        });
        Storage::disk('profile')->put("/$folder/$size.jpg", $img->encode());
    }

    /**
     * @param $name
     * @return string
     */
    private function formatLettersFromName($name) {
        $name_ascii = strtoupper(Str::ascii($name));
        $words = preg_split("/[\s,_-]+/", $name_ascii);
        if(count($words) >= 2) $str = $words[0][0].$words[1][0];
        else $str = substr($name_ascii, 0, 2);

        return $str;
    }

    /**
     * @param $image
     * @param $folder
     */
    public function update($image, $folder) {
        $img = Image::make($image);

        $this->resizeImage($img, $folder, self::LARGE);
        $this->resizeImage($img, $folder, self::MEDIUM);
        $this->resizeImage($img, $folder, self::SMALL);
    }

    /**
     * @param $img
     * @param $folder
     * @param $size
     */
    private function resizeImage($img, $folder, $size) {
        $img->fit($size, $size);

        Storage::disk('profile')->put("/$folder/$size.jpg", $img->encode('jpg', 90));
    }
}