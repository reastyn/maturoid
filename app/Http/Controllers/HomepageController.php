<?php

namespace App\Http\Controllers;

use App\Modules\Project\Models\Project;
use App\Modules\Project\Models\ProjectComment;
use App\Modules\Project\Models\ProjectFile;
use App\Modules\Project\Models\ProjectTask;
use App\Modules\Project\Models\ReservedProject;
use App\Notification;
use App\User;
use Auth;
use Carbon\Carbon;


class HomepageController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        if ($user->hasRole('student') && $user->project_id != null) {
            return $this->renderStudent($user);
        } else if ($user->hasRole(['opponent', 'manager'])) {
            return $this->renderManage($user);
        } else if ($user->hasRole('admin')) {
            return $this->renderAdmin($user);
        } else {
            return $this->renderVisitor($user);
        }
    }

    /**
     * @param $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function renderStudent($user)
    {
        $taskCount = ProjectTask::where('project_id', $user->project_id)->whereIn('state', [1, 0])->count();
        $commentCount = ProjectComment::where([['project_id', '=', $user->project_id], ['updated_at', '>=', Carbon::now()->subDay()]])->count();
        $completedTaskCount = ProjectTask::where('project_id', $user->project_id)->whereIn('state', [2, 3])->count();
        $tasks = ProjectTask::where('project_id', $user->project_id)->orderBy('state')->orderBy('due')->get();
        $project = Project::find($user->project_id);
        $report = ProjectFile::where(['project_id' => $user->project_id, 'type' => 2])->get();

        return view('homepage.student', ['taskCount' => $taskCount, 'commentCount' => $commentCount,
            'completedTaskCount' => $completedTaskCount, 'tasks' => $tasks, 'project' => $project, 'reportFiles' => $report]);
    }

    /**
     * @param $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function renderManage($user)
    {
        $projectIds = Project::where(['created_by' => $user->id, 'state' => 2])->pluck('id')->toArray();
        $reservedProjects = ReservedProject::with('user', 'project')->whereIn('project_id', $projectIds);
        $users = User::where('project_id', '!=', null)->where('opponent_id', $user->id)->orWhere('manager_id', $user->id);
        $projectCount = $users->count();
        $projects = Project::with('users', 'category')->whereIn('id', $users->pluck('project_id')->toArray())->get();
        $ownProjectCount = Project::where('created_by', $user->id)->count();

        return view('homepage.manage', ['reservedProjects' => $reservedProjects->get(), 'projectCount' => $projectCount,
            'ownProjectCount' => $ownProjectCount, 'projects' => $projects, 'reserveProjectsCount' => $reservedProjects->count()]);
    }

    /**
     * @param $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function renderAdmin($user)
    {
        $usersCount = User::count();
        $users = User::orderBy('created_at')->take(8)->get();
        $projects = Project::with('users', 'category')->orderBy('updated_at')->take(10)->get();
        $projectCount = Project::count();
        $reservedProjects = ReservedProject::with('user', 'project')->count();
        return view('homepage.admin', ['reservedProjects' => $reservedProjects, 'usersCount' => $usersCount,
            'projectCount' => $projectCount, 'users' => $users, 'projects' => $projects]);
    }

    /**
     * @param $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function renderVisitor($user)
    {
        $reservedProjects = Project::where('state', 2)->count();
        $users = User::orderBy('created_at')->take(8)->get();
        return view('homepage.visitor', ['projectCount' => Project::count(), 'usersCount' => User::count(),
            'reservedProjects' => $reservedProjects, 'users' => $users, 'fileCount' => ProjectFile::count(),
            'taskCount' => ProjectTask::count(), 'commentCount' => ProjectComment::count(), 'notificationCount' =>
            Notification::count()]);
    }
}
