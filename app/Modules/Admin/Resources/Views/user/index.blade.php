@extends('master')

@section('content')
    <div class="row">
        <div class="col-md-3">
            <div class="box box-primary">
                <div class="box-header with-border"><h3 class="box-title">Akce</h3></div>
                <div class="box-body container-fluid">
                    <div class="must-check r-disable">
                        <p>Přiřazení oponenta</p>
                        <form class="assignForm" action="{{ route('users.assign') }}">
                            <div class="form-group">
                                <select class="col-sm-8" name="opponent" id="opponents">
                                    <option></option>
                                    @include('admin::user.options', ['data' => $opponents])
                                </select>
                                <button type="submit" class="btn btn-primary pull-right col-sm-4">Přiřadit</button>
                            </div>
                        </form>

                        <form class="assignForm" action="{{ route('users.assign') }}">
                            <p>Přiřazení vedoucího</p>
                            <div class="form-group">
                                <select class="col-sm-8" name="manager" id="managers">
                                    <option></option>
                                    @include('admin::user.options', ['data' => $managers])
                                </select>
                                <button type="submit" class="btn btn-primary pull-right col-sm-4">Přiřadit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="box">
                <div class="box-body">
                    <table class="table table-bordered dataTable" id="users-table">
                        <thead>
                        <tr>
                            <th></th>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Oponent</th>
                            <th>Vedoucí</th>
                            <th>Akce</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@push('scripts')
<script>
    $(function () {
        $('#opponents').select2({
            placeholder: 'Vyberte oponenta',
            width: '66.66666667%',
            allowClear: true
        });
        $('#managers').select2({
            placeholder: 'Vyberte vedoucího',
            width: '66.66666667%',
            allowClear: true
        });

        // this is the id of the form
        $(".assignForm").submit(function (e) {
            $.ajax({
                type: "POST",
                url: $(this).attr('action'),
                data: {
                    assign: $(this).serializeArray(),
                    selected: selected
                },
                success: function() {
                    maturoid.notify('Úspěch', 'Vedoucí / oponenti byli úspěšně přiděleni uživatelům', 'success');
                },
                error: function() {
                    maturoid.notify('Něco se pokazilo..', 'Nevybrali jste vedoucího / oponenta', 'error');
                },
                complete: function () {
                    dataTable.ajax.reload();
                }
            });
            e.preventDefault();
        });

        var table = $('#users-table');
        var selected = [];
        table.on('click', '.deleteUser', function () {
            if (confirm('Opravdu chcete uživatele smazat?')) {
                $.ajax({
                    type: "POST",
                    url: '{!! route('users.destroy') !!}',
                    data: {
                        id: $(this).data('id')
                    },
                    complete: function () {
                        dataTable.ajax.reload();
                    }
                });
            }
        });
        table.on('change', 'input[type=checkbox]', function () {
            var id = parseInt(this.value);
            var index = $.inArray(id, selected);

            if (index === -1) {
                selected.push(id);
            } else {
                selected.splice(index, 1);
            }

            if (selected.length > 0) {
                $('.must-check').removeClass('r-disable');
            } else {
                $('.must-check').addClass('r-disable');
            }

            $(this).closest('tr').toggleClass('selected');
        });
        var dataTable = table.DataTable({
            pageLength: 50,
            autoWidth: false,
            processing: true,
            serverSide: true,
            ajax: '{!! route('users.data') !!}',
            order: [[1, 'asc']],
            "rowCallback": function (row, data) {
                if ($.inArray(data.DT_RowId, selected) !== -1) {
                    $(row).addClass('selected').find('input[type=checkbox]').attr('checked', true);
                }
            },
            columns: [
                {data: 'checkbox', name: 'checkbox', orderable: false, searchable: false, width: "1px"},
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'opponent', name: 'opponent', orderable: false, searchable: false},
                {data: 'manager', name: 'manager', orderable: false, searchable: false},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
    });
</script>
@endpush