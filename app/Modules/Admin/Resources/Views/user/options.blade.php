@foreach($data as $d)
    <option @if(isset($selected) && $selected == $d->id) selected @endif value="{{ $d->id }}">{{ $d->name }}</option>
@endforeach