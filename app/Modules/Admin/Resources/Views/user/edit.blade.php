@extends('master')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h2 id="title">{{ $user->name }}</h2>
        </div>
        <div class="box-body">
            <form method="POST"
                  action="{{ route('users.update', $user->id) }}">
                <input name="_method" type="hidden" value="PUT">
                {{ csrf_field() }}
                <div class="row form-group">
                    <div class="col-md-6">
                        <label for="name">Jméno </label>
                        <input type="text" class="form-control" name="name" id="name" value="{{ $user->name }}">
                    </div>
                    <div class="col-md-6">
                        <label for="email">Email </label>
                        <input type="text" class="form-control" name="email" id="email" value="{{ $user->email }}">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-6">
                        <label for="opponents">Oponent </label>
                        <select name="opponent_id" id="opponents">
                            <option></option>
                            @include('admin::user.options', ['data' => $opponents, 'selected' => $user->opponent_id])
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label for="managers">Vedoucí </label>
                        <select name="manager_id" id="managers">
                            <option></option>
                            @include('admin::user.options', ['data' => $managers, 'selected' => $user->manager_id])
                        </select>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-12">
                        <label for="roles">Role </label>
                        <select name="roles[]" id="roles" multiple>
                            <option></option>
                            @foreach($roles as $role)
                                <option @if($user->hasRole($role->role)) selected @endif value="{{ $role->role }}">{{ $role->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary pull-right">Uložit</button>
            </form>
        </div>
    </div>
@stop

@push('scripts')
<script>
    $(function () {
        $('#opponents').select2({
            placeholder: 'Vyberte oponenta',
            width: '100%',
            allowClear: true
        });
        $('#managers').select2({
            placeholder: 'Vyberte vedoucího',
            width: '100%',
            allowClear: true
        });
        $('#roles').select2({
            placeholder: 'Vyberte role',
            width: '100%',
            allowClear: true
        });
        var title = $('#title');
        $('#name').keyup(function () {
            title.text(this.value);
        })
    });
</script>
@endpush