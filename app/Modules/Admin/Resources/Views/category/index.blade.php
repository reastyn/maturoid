@extends('master')

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="form-group">
                <a href="{{ route('categories.create') }}" class="btn btn-primary">Nová kategorie</a>
            </div>
            <table class="table table-bordered dataTable" id="users-table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Název</th>
                    <th>Akce</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@stop

@push('scripts')
<script>
    $(function () {
        var table = $('#users-table');
        var selected = [];
        table.on('click', '.deleteUser', function () {
            if (confirm('Opravdu chcete uživatele smazat?')) {
                $.ajax({
                    type: "POST",
                    url: '{!! route('categories.destroy') !!}',
                    data: {
                        id: $(this).data('id')
                    },
                    complete: function () {
                        dataTable.ajax.reload();
                    }
                });
            }
        });
        var dataTable = table.DataTable({
            pageLength: 50,
            autoWidth: false,
            processing: true,
            serverSide: true,
            ajax: '{!! route('categories.data') !!}',
            columns: [
                {data: 'id', name: 'id', width: '20px'},
                {data: 'name', name: 'name'},
                {data: 'action', name: 'action', orderable: false, searchable: false, width: "150px"},
            ]
        });
    });
</script>
@endpush