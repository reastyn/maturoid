@extends('master')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h2 id="title">{{ isset($category) ? $category->name : 'Nová kategorie' }}</h2>
        </div>
        <div class="box-body">
            @if(isset($category))
                <form method="POST"
                      action="{{ route('categories.update', $category->id) }}">
                    <input name="_method" type="hidden" value="PUT">
                    @else
                        <form method="POST"
                              action="{{ route('categories.store') }}">
                            @endif
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="name">Název </label>
                                <input type="text" class="form-control" name="name" id="name"
                                       placeholder="Nová kategorie"
                                       value="{{ isset($category) ? $category->name : '' }}">
                            </div>
                            <button type="submit" class="btn btn-primary pull-right">Uložit</button>
                        </form>
        </div>
    </div>
@stop

@push('scripts')
<script>
    $(document).ready(function () {
        var title = $('#title');
        $('#name').keyup(function () {
            if (!$(this).val() == '') {
                title.text(this.value);
            } else {
                title.text('Nová kategorie');
            }
        });
    });
</script>
@endpush