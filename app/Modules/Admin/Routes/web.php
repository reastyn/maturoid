<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['middleware' => ['auth', 'role:admin'], 'prefix' => 'admin',
    'breadcrumb' => ['name' => 'Admin', 'url' => 'users.index']], function () {
//    Route::resource('users', 'UserController');
    Route::group(['breadcrumb' => 'Uživatelé'], function() {
        Route::get('users/data', 'UserController@data')->name('users.data');
        Route::post('users/assign', 'UserController@assign')->name('users.assign');
        Route::resource('users', 'UserController');
        Route::post('users/destroy', 'UserController@destroy')->name('users.destroy');
    });

    Route::group(['breadcrumb' => 'Kategorie'], function() {
        Route::get('categories/data', 'CategoryController@data')->name('categories.data');
        Route::resource('categories', 'CategoryController');
        Route::post('categories/destroy', 'UserController@destroy')->name('categories.destroy');
    });
});