<?php

namespace App\Modules\Admin\Http\Controllers;

use App\Modules\Auth\Role;
use App\Modules\Project\Models\Category;
use App\Notifications\UserNotification;
use App\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Datatables;
use URL;

class CategoryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin::category.index', ['categories' => Category::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin::category.edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Category::create($request->all());

        return redirect()->route('categories.index')->with([
            'title' => 'Úspěch',
            'body' => 'Kategorii se úspěšně podařilo vytvořit.',
            'type' => 'success'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin::category.edit', ['category' => Category::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::find($id);
        $category->update($request->all());

        return redirect()->route('categories.index')->with([
            'title' => 'Úspěch',
            'body' => 'Kategorii se úspěšně podařilo upravit.',
            'type' => 'success'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(Request $request)
    {
        return Category::destroy($request->id);
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data()
    {
        return Datatables::of(Category::all())
            ->setRowId('id')
            ->addColumn('action', function ($category) {
                return '<a href="' . route('categories.edit', $category->id) . '" class="btn btn-xs btn-primary">
                <i class="glyphicon glyphicon-edit"></i> Upravit</a> 
                <a data-id="' . $category->id . '" href="#" class="btn btn-xs btn-danger">
                <i class="glyphicon glyphicon-trash"></i> Smazat</a>';
            })
            ->make(true);
    }
}
