<?php

namespace App\Modules\Admin\Http\Controllers;

use App\Modules\Auth\Role;
use App\Notifications\UserNotification;
use App\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Datatables;
use URL;

class UserController extends Controller
{
    private $opponents;

    private $managers;

    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->opponents = User::getAllWithRole('opponent');
        $this->managers = User::getAllWithRole('manager');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin::user.index', ['opponents' => $this->opponents, 'managers' => $this->managers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Role::all();

        return view('admin::user.edit', ['user' => User::find($id), 'opponents' => $this->opponents, 'managers' => $this->managers, 'roles' => $roles]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->update($request->all());
        $user->syncRoles($request->roles);

        return redirect()->route('users.index')->with([
            'title' => 'Úspěch',
            'body' => 'Uživatele se podařilo úspěšně upravit',
            'type' => 'success'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(Request $request)
    {
        if (Auth::getUser()->id != $request->id) {
            User::destroy($request->id);
            return response('Success', 200);
        }

        return response('Forbidden', 400);
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data()
    {
        $users = User::with('roles');

        return Datatables::of($users)
            ->setRowId('id')
            ->addColumn('opponent', function ($user) {
                $opponent = User::find($user->opponent_id);
                if ($opponent != null) {
                    return '<img class="user-image" src="' . URL::to('storage/profile') . '/' . $opponent->profile . '/100.jpg"> ' . $opponent->name;
                }
                return '';
            })
            ->addColumn('manager', function ($user) {
                $manager = User::find($user->manager_id);
                if ($manager != null) {
                    return '<img class="user-image" src="' . URL::to('storage/profile') . '/' . $manager->profile . '/100.jpg"> ' . $manager->name;
                }
                return '';
            })
            ->addColumn('action', function ($user) {
                return '<a href="' . route('users.edit', $user->id) . '" class="btn btn-xs btn-primary">
                <i class="glyphicon glyphicon-edit"></i> Upravit</a> 
                <a data-id="' . $user->id . '" href="#" class="btn btn-xs btn-danger deleteUser">
                <i class="glyphicon glyphicon-trash"></i> Smazat</a>';
            })
            ->addColumn('checkbox', function ($user) {
                return '<input type="checkbox" value="' . $user->id . '">';
            })
            ->make(true);
    }

    /**
     * @param Request $request
     * @return Request
     */
    public function assign(Request $request)
    {
        if (isset($request->selected) && !empty($request->assign[0]['value'])) {
            if ($request->assign[0]['name'] == 'opponent') {
                $update = ['opponent_id' => $request->assign[0]['value']];
            } else if ($request->assign[0]['name'] == 'manager') {
                $update = ['manager_id' => $request->assign[0]['value']];
            } else {
                return response('Bad choice', 400);
            }

            User::whereIn('id', $request->selected)->update($update);
            return response('Success', 200);
        }
        return response('Bad choice', 400);
    }
}
