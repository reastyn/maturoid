<?php

// Password Reset routes
Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm');
Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('email.send');
Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm');
Route::post('password/reset', 'ResetPasswordController@reset')->name('reset');

Route::get('login', 'LoginController@showLoginForm');
Route::post('login', 'LoginController@login')->name('login');
Route::post('logout', 'LoginController@logout');
Route::get('logout', 'LoginController@logout')->name('logout');

// Registration Routes
Route::get('register', function () {
    return redirect('login#registerTab');
});
Route::post('register', 'RegisterController@register')->name('register');

// Social auth routes
Route::get('auth/{provider}', 'SocialAuthController@redirect')->name('authRedirect');
Route::get('auth/{provider}/callback', 'SocialAuthController@handle');

// Other routes (profile, generated account)
Route::group(['middleware' => 'auth'], function () {
    Route::group(['breadcrumb' => 'Profil'], function() {
        Route::post('profile/image', 'ProfileController@updateProfileImage')->name('profile.updateProfile');
    });

    Route::group(['breadcrumb' => 'Notifikace'], function() {
        Route::post('notifications', 'NotificationController@notificationRead');
        Route::post('notifications/markRead', 'NotificationController@allNotificationsRead')->name('notifications.markRead');
        Route::get('notifications', 'NotificationController@getNotifications')->name('notifications');
        Route::get('notifikace', 'NotificationController@index')->name('notifications.index');
    });
});