<?php

namespace App\Modules\Auth\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{

    public function messages()
    {
        return [
            'email.required' => 'Email je povinný',
            'email.email' => 'Email není ve správném formátu',
            'email.exists' => 'Zadaný email v databázi neexistuje',
            'password.required' => 'Heslo je povinné'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|exists:users',
            'password' => 'required',
        ];
    }

    public function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();

        $validator->after(function () use ($validator) {
            $email = $this->input('email');
            if (!User::where('email', $email)->first()) {
                $validator->errors()->add('email', 'Zadaný email v databázi neexistuje');
                return $validator;
            }
            if (!User::where('email',
                $email)->first()->hasPassword()
            ) {
                $validator->errors()->add('email', 'Tento uživatel se již přihlásil přes sociální síť');
                return $validator;
            }
        });

        return $validator;
    }
}
