<?php

namespace App\Modules\Auth\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{

    protected $redirectRoute = 'register';

    protected $errorBag = 'register';

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'email.required' => 'Email je povinný',
            'email.email' => 'Email není ve správném formátu',
            'email.unique' => 'Zadaný email v databázi již existuje',
            'password.required' => 'Heslo je povinné',
            'password.min' => 'Minimální počet znaků je 6',
            'password.confirmed' => 'Hesla se neshodují'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:users',
            'name' => 'required|max:255',
            'password' => 'required|min:6|confirmed',
        ];
    }
}
