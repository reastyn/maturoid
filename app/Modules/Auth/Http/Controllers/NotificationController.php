<?php

namespace App\Modules\Auth\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class NotificationController extends Controller
{
    public function notificationRead(Request $request)
    {
        $notification = $request->user()->notifications()->where('id', $request->id)->first();

        if ($notification) {
            $notification->markAsRead();
        }
    }

    public function getNotifications(Request $request)
    {
        $notifications = $request->user()->notifications()->paginate(10);
        if($notifications->isEmpty()) {
            abort(404);
        }
        return view('auth::notifications', ['notifications' => $notifications]);
    }

    public function allNotificationsRead(Request $request)
    {
        return $request->user()->notifications->markAsRead();
    }

    public function index()
    {
        return view('auth::notifications.index');
    }
}
