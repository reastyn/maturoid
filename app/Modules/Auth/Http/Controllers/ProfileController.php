<?php

namespace App\Modules\Auth\Http\Controllers;

use App\Helpers\ProfileImage;
use App\Modules\Auth\Jobs\UpdateUserImage;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;
use Response;
use Storage;
use Validator;

class ProfileController extends Controller
{

    /**
     * @param Request $request
     * @return string
     */
    public function updateProfileImage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file' => 'image|max:1024',
        ], ['file.image' => 'Soubor musí být obrázek.', 'file.max' => 'Obrázek musí mít pod 1 MB.']);

        if ($validator->fails()) {
            return Response::json([
                'error' => $validator->messages()->first(),
                'code' => 400
            ], 400);
        }

        dispatch(new UpdateUserImage(new ProfileImage, Auth::user(), $request->file('file')->getRealPath()));
    }
}
