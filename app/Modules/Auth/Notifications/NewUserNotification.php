<?php

namespace App\Modules\Auth\Notifications;

use App\Notifications\UserNotification;
use Illuminate\Notifications\Messages\MailMessage;

class NewUserNotification extends UserNotification
{

    /**
     * ProjectNotification constructor.
     * @param $props
     */
    public function __construct($props)
    {
        $this->icon = 'user-circle';
        parent::__construct($props);
    }


    /**
     * Get the notification's delivery channels.
     *
     * @return array
     */
    public function via()
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Od teď můžete používat aplikaci maturoid.')
            ->greeting('Váš účet byl úspěšně založen.')
            ->action('Otevřít maturoid', route('home'));
    }
}
