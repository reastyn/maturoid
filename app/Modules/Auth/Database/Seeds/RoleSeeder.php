<?php

namespace App\Modules\Auth\Database\Seeds;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::updateOrCreate(['role' => 'visitor', 'name' => 'Návštěvník']);
        Role::updateOrCreate(['role' => 'student', 'name' => 'Student']);
        Role::updateOrCreate(['role' => 'admin', 'name' => 'Admin']);
        Role::updateOrCreate(['role' => 'opponent', 'name' => 'Oponent']);
        Role::updateOrCreate(['role' => 'manager', 'name' => 'Vedoucí']);
    }
}
