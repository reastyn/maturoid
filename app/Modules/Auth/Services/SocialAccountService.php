<?php

namespace App\Modules\Auth\Services;

use App\SocialAccount;
use App\User;
use Laravel\Socialite\Contracts\Provider;

class SocialAccountService
{
    /**
     * @param Provider $provider
     * @return static
     */
    public function createOrGetUser(Provider $provider)
    {
        $providerUser = $provider->user();
        $providerName = class_basename($provider);

        $account = SocialAccount::whereProvider($providerName)
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if ($account) {
            return $account->user;
        } else {

            $account = new SocialAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => $providerName
            ]);

            $user = User::whereEmail($providerUser->getEmail())->first();

            if (!$user) {
                $user = User::createUser($providerUser->getName(), $providerUser->getEmail());
            }

            $account->user()->associate($user);
            $account->save();

            return $user;

        }
    }
}