@extends('master')

@section('content')
    <div class="row">
        <div class="col-md-3">
            <div class="box box-primary">
                <div class="box-header with-border"><h3 class="box-title">Akce</h3></div>
                <div class="box-body text-center">
                    <p id="notCount">Nepřečtených notifikací: {{ Auth::user()->notifications->where('read_at', null)->count() }}</p>
                    <a href="#" class="btn btn-primary markAllRead">Označit vše jako přečtené</a>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="box">
                <div class="box-body notifications">
                    @include('auth::notifications', ['notifications' => Auth::user()->notifications])
                </div>
                <div class="text-center box-footer">
                    <a class="btn btn-primary toggle">Načíst další</a>
                    <i class="fa fa-refresh fa-spin toggle" style="display: none;"></i>
                    <span id="noMore" style="display: none;">Žádné další notifikace nenalezeny.</span>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script>
    $(document).ready(function () {
        var win = $(window),
            loading = $('.toggle'),
            page = 2;

        maturoid.onNotificationClick();

        $('.markAllRead').on('click', function() {
            $('.notifications .notification a').removeClass('unread');
            $('#notCount').text('Žádné nepřečtené notifikace.');
        });

        $('.btn.toggle').on('click', function () {
            loading.toggle();

            $.ajax({
                url: '{{ route('notifications') }}',
                method: 'GET',
                data: {
                    page: page
                },
                dataType: 'html',
                error: function() {
                    loading.hide();
                    $('#noMore').show();
                },
                success: function (html) {
                    page++;
                    $('.notifications').append(html);
                    loading.toggle();
                }
            })
        });
    })
</script>
@endpush