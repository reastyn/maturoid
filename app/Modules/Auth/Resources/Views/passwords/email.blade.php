@extends('auth::layouts.master')

@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <!-- Password recovery -->
    <form method="POST" action="{{ route('email.send') }}">
        {{ csrf_field() }}
        <div class="panel panel-body login-form">
            <div class="text-center">
                <div class="icon-object border-warning text-warning"><i class="icon-spinner11"></i>
                </div>
                <h5 class="content-group">Zapomenuté heslo
                    <small class="display-block">Odešleme vám email co nejdříve</small>
                </h5>
            </div>

            <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
                <input name="email" type="email" class="form-control" placeholder="E-mail"
                       value="{{ old('email') }}">
                <div class="form-control-feedback">
                    <i class="icon-mail5 text-muted"></i>
                </div>
                @if ($errors->has('email'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                @endif
            </div>

            <button type="submit" class="btn bg-blue btn-block">Resetovat heslo <i
                        class="icon-arrow-right14 position-right"></i></button>
        </div>
    </form>
    <!-- /password recovery -->
@endsection
