
    <ul class="menu">
        @foreach($notifications as $n)
            <li class="notification">
                <a href="{{ $n->data['url'] }}" data-id="{{ $n->id }}" @if(!$n->read_at) class="unread" @endif>
                    <div class="pull-left"><i class="fa fa-{{$n->data['icon'] or 'exclamation-circle'}} text-aqua"></i></div>
                    <small class="pull-right"><i class="fa fa-clock-o"></i> {{ $n->created_at->diffForHumans() }}
                    </small>
                    <h4>
                        {{ $n->data['title'] }}
                    </h4>
                    <p>{{ $n->data['body'] }}</p>
                </a>
            </li>
        @endforeach
    </ul>