@extends('auth::layouts.master')

@section('content')
    <!-- Tabbed form -->
    <div class="tabbable panel login-form width-400">
        <ul class="nav nav-tabs nav-justified">
            <li class="active"><a href="#loginTab" data-toggle="tab"><h6>Přihlásit se</h6></a></li>
            <li><a href="#registerTab" data-toggle="tab"><h6>Registrovat</h6></a></li>
        </ul>

        <div class="tab-content panel-body">
            <div class="tab-pane fade in active" id="loginTab">
                <form id="login" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}
                    <div class="text-center">
                        <div class="icon-object border-warning-400 text-warning-400"><i class="icon-cloud-upload2"></i>
                        </div>
                        <h5 class="content-group-lg">Maturoid
                            <small class="display-block">Přihlašte se do svého účtu</small>
                        </h5>
                    </div>

                    <div class="form-group has-feedback has-feedback-left {{ $errors->has('email') ? ' has-error' : '' }}">
                        <input name="email" type="email" class="form-control" placeholder="Email"
                               value="{{ old('email') }}">
                        @if ($errors->has('email'))
                            <span id="email-error" class="help-block error-help-block">
                                {{ $errors->first('email') }}
                            </span>
                        @endif
                        <div class="form-control-feedback">
                            <i class="icon-user text-muted"></i>
                        </div>
                    </div>

                    <div class="form-group has-feedback has-feedback-left {{ $errors->has('password') ? ' has-error' : '' }}">
                        <input type="password" name="password" class="form-control" placeholder="Password">
                        @if ($errors->has('password'))
                            <span id="password-error" class="help-block error-help-block">
                                {{ $errors->first('password') }}
                            </span>
                        @endif
                        <div class="form-control-feedback">
                            <i class="icon-lock2 text-muted"></i>
                        </div>
                    </div>

                    <div class="form-group login-options">
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="checkbox-inline">
                                    <input name="remember" type="checkbox" class="styled">
                                    Pamatuj si mě
                                </label>
                            </div>

                            <div class="col-sm-6 text-right">
                                <a href="{{ route('reset') }}">Zapomenuté heslo?</a>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn bg-blue btn-block">Přihlásit se <i
                                    class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </form>

                <div class="content-divider text-muted form-group"><span>nebo se přihlašte přes</span></div>
                <ul class="list-inline form-group list-inline-condensed text-center">
                    <li><a href="{{ route('authRedirect', ['provider' => 'google']) }}"
                           class="btn border-danger text-danger btn-flat btn-icon btn-rounded"><i
                                    class="icon-google"></i></a></li>
                    <li><a href="{{ route('authRedirect', ['provider' => 'facebook']) }}"
                           class="btn border-indigo text-indigo btn-flat btn-icon btn-rounded"><i
                                    class="icon-facebook"></i></a></li>
                </ul>
            </div>

            <div class="tab-pane fade" id="registerTab">
                <form id="register" method="post" action="{{ route('register') }}">
                    {{ csrf_field() }}
                    <div class="text-center">
                        <div class="icon-object border-success text-success"><i class="icon-plus3"></i></div>
                        <h5 class="content-group">Vytvořit nový účet
                            <small class="display-block">Všechna pole jsou povinná</small>
                        </h5>
                    </div>

                    <div class="form-group has-feedback has-feedback-left{{ $errors->register->has('email') ? ' has-error' : '' }}">
                        <input name="email" type="text" class="form-control" placeholder="E-mail">
                        @if ($errors->register->has('email'))
                            <span id="email-error" class="help-block error-help-block">
                                {{ $errors->register->first('email') }}
                            </span>
                        @endif
                        <div class="form-control-feedback">
                            <i class="icon-mention text-muted"></i>
                        </div>
                    </div>

                    <div class="form-group has-feedback has-feedback-left{{ $errors->register->has('name') ? ' has-error' : '' }}">
                        <input name="name" type="text" class="form-control" placeholder="Jméno">
                        @if ($errors->register->has('name'))
                            <span id="name-error" class="help-block error-help-block">
                                {{ $errors->register->first('name') }}
                            </span>
                        @endif
                        <div class="form-control-feedback">
                            <i class="icon-user-check text-muted"></i>
                        </div>
                    </div>

                    <div class="form-group has-feedback has-feedback-left{{ $errors->register->has('password') ? ' has-error' : '' }}">
                        <input name="password" type="password" class="form-control" placeholder="Heslo">
                        @if ($errors->register->has('password'))
                            <span id="password-error" class="help-block error-help-block">
                                {{ $errors->register->first('password') }}
                            </span>
                        @endif
                        <div class="form-control-feedback">
                            <i class="icon-user-lock text-muted"></i>
                        </div>
                    </div>

                    <div class="form-group has-feedback has-feedback-left{{ $errors->register->has('password_confirmation') ? ' has-error' : '' }}">
                        <input name="password_confirmation" type="password" class="form-control" placeholder="Zopakujte heslo">
                        @if ($errors->register->has('password_confirmation'))
                            <span id="password_confirmation-error" class="help-block error-help-block">
                                {{ $errors->register->first('password_confirmation') }}
                            </span>
                        @endif
                        <div class="form-control-feedback">
                            <i class="icon-loop4 text-muted"></i>
                        </div>
                    </div>

                    <button type="submit" class="btn bg-indigo-400 btn-block">Registrovat <i
                                class="icon-circle-right2 position-right"></i></button>
                </form>
            </div>
        </div>
    </div>
    @push('scripts')
    {!! JsValidator::formRequest('App\Modules\Auth\Http\Requests\LoginRequest', '#login') !!}
    {!! JsValidator::formRequest('App\Modules\Auth\Http\Requests\RegisterRequest', '#register') !!}
    <script>
        $(function(){
            var hash = window.location.hash;
            hash && $('ul.nav a[href="' + hash + '"]').tab('show');

            $('.nav-tabs a').click(function (e) {
                $(this).tab('show');
                var scrollmem = $('body').scrollTop() || $('html').scrollTop();
                window.location.hash = this.hash;
                $('html,body').scrollTop(scrollmem);
            });
        });
    </script>
    @endpush
@endsection
