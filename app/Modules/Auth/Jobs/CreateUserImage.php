<?php

namespace App\Modules\Auth\Jobs;

use App\Helpers\ProfileImage;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateUserImage implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var ProfileImage
     */
    protected $profileImage;

    /**
     * @var User
     */
    protected $user;

    /**
     * CreateUserImage constructor.
     * @param ProfileImage $profileImage
     * @param User $user
     */
    public function __construct(ProfileImage $profileImage, User $user)
    {
        $this->profileImage = $profileImage;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $folder = $this->profileImage->generate($this->user->getAttribute('name'));
        $this->user->update(['profile' => $folder]);
    }
}
