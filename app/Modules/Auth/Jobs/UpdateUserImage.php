<?php

namespace App\Modules\Auth\Jobs;

use App\Helpers\ProfileImage;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateUserImage implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $profileImage;

    protected $user;

    protected $image;

    public function __construct(ProfileImage $profileImage, User $user, $image)
    {
        $this->profileImage = $profileImage;
        $this->user = $user;
        $this->image = $image;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->profileImage->update($this->image, $this->user->profile);
    }
}
