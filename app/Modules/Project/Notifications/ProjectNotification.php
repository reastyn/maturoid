<?php

namespace App\Modules\Project\Notifications;

use App\Modules\Project\Models\Project;
use App\Notifications\UserNotification;
use Illuminate\Notifications\Messages\MailMessage;

class ProjectNotification extends UserNotification
{
    /**
     * @var Project
     */
    private $project;

    /**
     * @var string
     */
    private $username;

    /**
     * ProjectNotification constructor.
     * @param $props
     * @param $project
     * @param $username
     */
    public function __construct($props, $project, $username)
    {
        $this->icon = 'plus-circle';
        parent::__construct($props);
        $this->project = $project;
        $this->username = $username;
    }


    /**
     * Get the notification's delivery channels.
     *
     * @return array
     */
    public function via()
    {
        return ['mail', 'broadcast', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Uživatel ' . $notifiable->name . ' vytvořil(a) nový projekt')
                    ->greeting('Projekt: ' . $this->project->name)
                    ->line('Byl vytvořen nový projekt žákem ' . $notifiable->name . ' u kterého jste přidělen(a) jako vedoucí / oponent.')
                    ->action('Otevřít projekt', route('project.show', $this->project->id));
    }
}
