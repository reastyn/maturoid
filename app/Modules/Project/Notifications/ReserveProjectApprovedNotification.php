<?php

namespace App\Modules\Project\Notifications;

use App\Modules\Project\Models\Project;
use App\Notifications\UserNotification;
use Illuminate\Notifications\Messages\MailMessage;

class ReserveProjectApprovedNotification extends UserNotification
{
    private $reserved;

    /**
     * ProjectNotification constructor
     * @param $reserved
     */
    public function __construct($reserved)
    {
        $this->title = 'Gratulujeme';
        $this->body = 'Vámi rezervovaný projekt byl schválen, můžete na něm začít pracovat!';
        $this->type = 'success';
        $this->icon = 'calendar-check-o';
        $this->url = route('project.my');
        $this->reserved = $reserved;
    }


    /**
     * Get the notification's delivery channels.
     *
     * @return array
     */
    public function via()
    {
        return ['mail', 'broadcast', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Rezervovaný projekt byl schválen!')
            ->greeting('Vámi rezervovaný projekt byl schválen')
            ->line('Nyní na něm můžete začít pracovat!')
            ->action('Otevřít projekt', route('project.show', $this->reserved->project_id));
    }
}
