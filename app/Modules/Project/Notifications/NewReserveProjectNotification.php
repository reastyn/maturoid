<?php

namespace App\Modules\Project\Notifications;

use App\Modules\Project\Models\Project;
use App\Notifications\UserNotification;
use Illuminate\Notifications\Messages\MailMessage;

class NewReserveProjectNotification extends UserNotification
{
    private $reserved;

    private $user;

    /**
     * ProjectNotification constructor
     * @param $reserved
     */
    public function __construct($reserved, $user)
    {
        $this->title = 'Uživatel ' . $user->name . ' si chce rezervovat Vámi založený projekt';
        $this->body = 'Podívejte se prosím do správy a popřípadě mu ho odsouhlaste!';
        $this->type = 'success';
        $this->icon = 'cloud-download';
        $this->url = route('reserveProject.manage');
        $this->user = $user;
        $this->reserved = $reserved;
    }


    /**
     * Get the notification's delivery channels.
     *
     * @return array
     */
    public function via()
    {
        return ['mail', 'broadcast', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject($this->title)
            ->greeting($this->body)
            ->line('Pokud již nechcete nechat projekt k vybrání změňte mu prosím stav na Rozpracovaný projekt.')
            ->action('Otevřít správu projektů', route('reserveProject.manage'));
    }
}
