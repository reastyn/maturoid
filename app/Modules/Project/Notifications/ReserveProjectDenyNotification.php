<?php

namespace App\Modules\Project\Notifications;

use App\Modules\Project\Models\Project;
use App\Notifications\UserNotification;
use Illuminate\Notifications\Messages\MailMessage;

class ReserveProjectDenyNotification extends UserNotification
{
    private $reserved;

    /**
     * ProjectNotification constructor
     * @param $reserved
     */
    public function __construct($reserved)
    {
        $this->title = 'Neschváleno';
        $this->body = 'Vámi rezervovaný projekt nebyl schválen, zkuste se zeptat učitele nebo založit vlastní';
        $this->type = 'danger';
        $this->icon = 'times-circle-o';
        $this->url = route('reserveProject.index');
        $this->reserved = $reserved;
    }


    /**
     * Get the notification's delivery channels.
     *
     * @return array
     */
    public function via()
    {
        return ['mail', 'broadcast', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Vámi rezervovaný projekt nebyl schválen')
            ->greeting('Vámi rezervovaný projekt nebyl schválen, zkuste se zeptat učitele nebo založit vlastní')
            ->line('Nyní na něm můžete začít pracovat!')
            ->action('Založit vlastní', route('makeProject.index'));
    }
}
