<?php

namespace App\Modules\Project\Notifications;

use App\Modules\Project\Models\Project;
use App\Notifications\UserNotification;
use Illuminate\Notifications\Messages\MailMessage;

class StateChangeProjectNotification extends UserNotification
{
    /**
     * @var Project
     */
    private $project;

    /**
     * ProjectNotification constructor.
     */
    public function __construct($project)
    {
        $this->icon = 'check-circle';
        $this->project = $project;
        $this->type = 'success';
        $this->url = route('project.my');
        $state = Project::$state;
        switch ($project->state) {
            case $state[1]:
                $this->title = 'Oslavujeme!';
                $this->body = 'Váš maturitní projekt byl schválen.';
                break;
            case $state[3]:
                $this->title = 'Gratulace';
                $this->body = 'Váš maturitní projekt byl označen jako dokončený.';
                break;
            default:
                $this->title = 'Stav projektu byl změněn.';
                $this->body = 'Zkontrolujte jak se změnil váš projekt';
        }
    }


    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via()
    {
        return ['mail', 'broadcast', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject($this->title)
            ->greeting($this->body)
            ->action('Otevřít projekt', route('project.show', $this->project->id));
    }
}
