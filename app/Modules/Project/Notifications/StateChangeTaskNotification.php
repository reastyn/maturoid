<?php

namespace App\Modules\Project\Notifications;

use App\Modules\Project\Models\Project;
use App\Notifications\UserNotification;
use Illuminate\Notifications\Messages\MailMessage;

class StateChangeTaskNotification extends UserNotification
{
    /**
     * @var Project
     */
    private $task;

    /**
     * ProjectNotification constructor.
     * @param $task
     * @param $state
     * @param $projectId
     */
    public function __construct($task, $state, $projectId)
    {
        $this->task = $task;
        $this->type = 'success';
        $this->url = route('project.show', ['id' => $projectId, '#tasks']);
        switch ($state) {
            case 2:
                $this->title = 'Žák dokončil úkol';
                $this->body = 'Zkontrolujte ho prosím a popřípadě odsouhlaste.';
                $this->icon = 'check-circle';
                break;
            case 3:
                $this->title = 'Úkol schválen';
                $this->body = 'Úkol zadaný učitelem / vedoucím byl úspěšně schválen.';
                $this->icon = 'check-square';
                break;
            case 0:
                $this->title = 'Urgentní úkol';
                $this->body = 'Vypracujte úkol co nejrychleji a odklikněte ke schválení';
                $this->icon = 'exclamation-circle';
                $this->type = 'warning';
                break;
            case 1:
                $this->title = 'Úkol nebyl správně dokončen';
                $this->body = 'Podívejte se prosím na správné vypracování úkolu';
                $this->icon = 'times';
                $this->type = 'warning';
                break;
        }
    }


    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via()
    {
        return ['mail', 'broadcast', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject($this->title)
            ->greeting($this->body)
            ->action('Otevřít projekt', $this->url);
    }
}
