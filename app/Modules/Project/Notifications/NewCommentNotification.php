<?php

namespace App\Modules\Project\Notifications;

use App\Modules\Project\Models\Project;
use App\Notifications\UserNotification;
use Illuminate\Notifications\Messages\MailMessage;

class NewCommentNotification extends UserNotification
{
    /**
     * @var Project
     */
    private $project;

    /**
     * ProjectNotification constructor.
     */
    public function __construct($props, $project)
    {
        $this->icon = 'commenting';
        parent::__construct($props);
        $this->project = $project;
    }


    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via()
    {
        return ['mail', 'broadcast', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Na projektu "' . $this->project->name . '" přibyl nový komentář')
            ->greeting('Nový komentář na projektu ' . $this->project->name)
            ->action('Otevřít projekt', route('project.show', $this->project->id));
    }
}
