<?php

namespace App\Modules\Project\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ProjectTask extends Model
{
    protected $guarded = ['id'];

    public static $state = [
        1 => ['name' => 'Rozpracovaný', 'icon' => 'fa-circle-o', 'class' => 'text-lime', 'done' => false],
        2 => ['name' => 'Ke schválení', 'icon' => 'fa-dot-circle-o', 'class' => 'text-purple', 'done' => true],
        0 => ['name' => 'Urgentní', 'icon' => 'fa-exclamation-circle', 'class' => 'text-orange', 'done' => false],
        3 => ['name' => 'Dokončen', 'icon' => 'fa-check-circle', 'class' => 'text-green', 'done' => true]
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function getDueAttribute()
    {
        return $this->attributes['due'] ? Carbon::createFromFormat('Y-m-d', $this->attributes['due'])->format('d.m.Y') : null;
    }

    public function setDueAttribute($due)
    {
        $this->attributes['due'] = !empty($due) ? Carbon::createFromFormat('d.m.Y', $due)->format('Y-m-d') : null;
    }

    public function getStateAttribute()
    {
        return self::$state[$this->attributes['state']];
    }
}