<?php

namespace App\Modules\Project\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Project extends Model
{
    use Notifiable;
    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @var array
     */
    public static $state = [
        0 => 'Projekt čeká na schválení',
        1 => 'Rozpracovaný projekt',
        2 => 'Projekt k výběru',
        3 => 'Dokončený projekt',
        4 => 'Archivovaný projekt'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function owner()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getStateAttribute($value)
    {
        return self::$state[$value];
    }

    /**
     * Project  : Comments
     * 1        : N
     */
    public function comments()
    {
        return $this->hasMany(ProjectComment::class);
    }

    public function files()
    {
        return $this->hasMany(ProjectFile::class);
    }

    public function images()
    {
        return $this->files()->where('type', 1);
    }

    public function printscreen()
    {
        return $this->files()->where('type', 1)->where('original_name', 'like', '%projekt%');
    }
}
