<?php

namespace App\Modules\Project\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class ProjectComment extends Model
{
    protected $guarded = ['id'];

    protected $appends = ['human_created_at'];

    /**
     * Get the administrator flag for the user.
     *
     * @return bool
     */
    public function getHumanCreatedAtAttribute()
    {
        return $this->created_at->diffForHumans();
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function project() {
        return $this->belongsTo(Project::class);
    }
}
