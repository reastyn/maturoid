<?php

namespace App\Modules\Project\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = ['id'];

    public function projects()
    {
        return $this->hasMany(Project::class);
    }
}
