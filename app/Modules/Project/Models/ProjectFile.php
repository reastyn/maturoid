<?php

namespace App\Modules\Project\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectFile extends Model
{
    protected $guarded = ['id'];

    protected $type = [
        0 => 'Soubor k projektu',
        1 => 'Galerie',
        2 => 'Posudky',
        3 => 'Projekt'
    ];

    public static function getValidationRulesByType($type)
    {
        switch ($type) {
            case 0:
                return [
                    'file' => 'max:8192|file|required'
                ];
            case 1:
                return [
                    'file' => 'max:8192|file|required|image'
                ];
            case 2:
            case 3:
                return [
                    'file' => 'file|required'
                ];
            default:
                return [
                    'type' => 'required'
                ];
        }
    }
}
