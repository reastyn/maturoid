<?php

namespace App\Modules\Project\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class CommentChange implements ShouldBroadcast
{
    use SerializesModels;

    public $projectId;

    /**
     * Create a new event instance.
     * @param $projectId
     */
    public function __construct($projectId)
    {
        $this->projectId = $projectId;
    }

    public function broadcastOn()
    {
        return new PrivateChannel('project.'.$this->projectId);
    }
}