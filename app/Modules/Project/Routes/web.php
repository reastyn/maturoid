<?php

use App\Helpers\Utils;

Route::group(['middleware' => 'auth', 'breadcrumb' => ['name' => 'Maturitní projekty', 'url' => 'project.index']], function () {
    Route::group(['breadcrumb' => 'Seznam'], function () {
        Route::resource('maturitni-projekty', 'ListController', ['names' => Utils::getResourceRoutesForNameHelper('project')]);
    });

    Route::get('tasks', 'ProjectTaskController@getTasks')->name('tasks');

    Route::group(['breadcrumb' => 'Vlastní projekt', 'middleware' => ['hasNoProject', 'canMakeOwnProject']], function () {
        Route::get('vlastni-projekt', 'MakeProjectController@index')->name('makeProject.index');
        Route::post('vlastni-projekt', 'MakeProjectController@store')->name('makeProject.store');
    });

    Route::group(['breadcrumb' => 'Projekt'], function () {
        Route::resource('projekt', 'ProjectController', ['names' => Utils::getResourceRoutesForNameHelper('project')]);
        Route::get('muj-projekt/{hash?}', 'ProjectController@showMyProject')->name('project.my');
        Route::post('projekt/smazat', 'ProjectController@destroy')->name('project.destroy');
        Route::group(['middleware' => 'uploadFileProject'], function() {
            Route::post('projekt/upload/{projekt}/{type?}', 'ProjectController@upload')->name('project.upload');
            Route::post('projekt/uploadProject/{projekt}/{type?}', 'ProjectController@uploadProject')->name('project.uploadProject');
            Route::post('projekt/smazSoubor/{projekt}/{type?}', 'ProjectController@destroyUpload')->name('project.destroyUpload');
            Route::get('projekt/ziskatSoubory/{projekt}/{type?}', 'ProjectController@getUploads')->name('project.getUploads');
        });
        Route::get('projekt/schvalit/{projekt}', 'ProjectController@approve')->name('project.approve');
    });

    Route::group(['breadcrumb' => 'Projekty k vybrání', 'middleware' => 'hasNoProject'], function () {
        Route::get('projekty-k-vybrani', 'ReserveProjectController@index')->name('reserveProject.index');
        Route::get('projekty-k-vybrani/rezervovat/{project}', 'ReserveProjectController@reserve')->name('project.reserve');
    });

    Route::group(['breadcrumb' => 'Správa rezervací projektů', 'middleware' => 'role:admin,manager,opponent'], function() {
        Route::get('sprava-rezervaci-projektu', 'ReserveProjectController@manage')->name('reserveProject.manage');
        Route::get('schvalit/{reserved}', 'ReserveProjectController@approve')->name('reserveProject.approve');
        Route::get('zamitnout/{reserved}', 'ReserveProjectController@deny')->name('reserveProject.deny');
    });

    Route::group(['breadcrumb' => 'Rezervace projektů'], function() {
        Route::get('rezervace-projektu', 'ReserveProjectController@manage')->name('reserveProject.manage');
    });

    Route::group(['breadcrumb' => 'Archivované projekty'], function() {
        Route::get('projekty/archivovane/{category?}', 'ArchiveProjectController@index')->name('archiveProject.index');
        Route::get('projekt/archivovany/{projekt}', 'ArchiveProjectController@show')->name('archiveProject.show');
    });

    // Api routes for project
    Route::group(['middleware' => 'canEditProject'], function() {
        Route::get('projekt/{projekt}/komentar', 'ProjectCommentController@get')->name('project.comments');
        Route::post('projekt/{projekt}/komentar', 'ProjectCommentController@store')->name('project.comment.store');
        Route::post('projekt/{projekt}/komentar/smazat', 'ProjectCommentController@delete')->name('project.comment.delete');

        Route::post('projekt/{projekt}/ukol', 'ProjectTaskController@store')->name('project.task.store');
        Route::post('projekt/{projekt}/ukol/smazat', 'ProjectTaskController@delete')->name('project.task.delete');
        Route::post('projekt/{projekt}/ukol/stav', 'ProjectTaskController@changeState')->name('project.task.state');
        Route::get('projekt/{projekt}/ukoly', 'ProjectTaskController@get')->name('project.tasks');
    });

    Route::group(['breadcrumb' => 'Správa projektů'], function () {
        Route::get('sprava-projektu', 'ManageProjectController@index')->name('manageProject.index');
        Route::get('sprava-projektu/data', 'ManageProjectController@data')->name('manageProject.data');
    });
});