<?php

namespace App\Modules\Project\Helpers;

use App\Modules\Project\Models\ProjectFile;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManager;
use App\Modules\Project\Models\Files;
use Storage;

class ProjectRepository
{
    public function upload($data, $id, $type)
    {
        $file = $data['file'];
        $originalName = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        $originalNameWithoutExt = substr($originalName, 0, strlen($originalName) - strlen($extension) - 1);

        $filename = $originalNameWithoutExt;
        $allowed_filename = $this->createUniqueFilename($filename, $extension);

        Storage::disk('project')->put($allowed_filename, File::get($file));

        $sFile = new ProjectFile;
        $sFile->filename = $allowed_filename;
        $sFile->original_name = $originalName;
        $sFile->project_id = $id;
        $sFile->type = $type;
        $sFile->size = $file->getSize();
        $sFile->save();

        return Response::json([
            'error' => false,
            'code' => 200
        ], 200);

    }

    public function createUniqueFilename($filename, $extension)
    {
        if (Storage::disk('project')->exists("$filename.$extension")) {
            // Generate token for image
            $imageToken = substr(sha1(mt_rand()), 0, 5);
            return $this->createUniqueFilename($filename . '-' . $imageToken, $extension);
        }

        return $filename . '.' . $extension;
    }

    /**
     * Delete Image From Session folder, based on original filename
     */
    public function delete($unique, $id)
    {
        $sFile = ProjectFile::where('filename', $unique)->where('project_id', $id)->first();

        if (empty($sFile) || !Storage::disk('project')->exists($sFile['filename'])) {
            return Response::json([
                'error' => true,
                'code' => 400
            ], 400);
        }

        Storage::disk('project')->delete($sFile['filename']);
        $sFile->delete();
        return Response::json([
            'error' => false,
            'code' => 200
        ], 200);
    }
}