<?php

namespace App\Modules\Project\Jobs;

use App\Modules\Project\Models\Project;
use App\Modules\Project\Notifications\NewCommentNotification;
use App\Modules\Project\Notifications\NewTaskNotification;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;

class NotifyAboutNewTask implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;


    protected $project;

    /**
     * @param Project $project
     */
    public function __construct(Project $project)
    {
        $this->project = $project;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $users = User::where(['project_id' => $this->project->id])->get();

        Notification::send($users,
            new NewTaskNotification([
                'title' => 'Nový úkol',
                'body' => 'Na projektu ' . $this->project->name . ' je nový úkol',
                'type' => 'info', 'url' => route('project.show', $this->project->id) . '#tasks'],
                $this->project));
    }
}
