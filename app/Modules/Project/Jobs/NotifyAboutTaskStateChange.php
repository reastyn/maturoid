<?php

namespace App\Modules\Project\Jobs;

use App\Modules\Project\Models\Project;
use App\Modules\Project\Notifications\StateChangeTaskNotification;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use Notification;

class NotifyAboutTaskStateChange implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $task;

    private $state;

    private $projectId;

    /**
     * @param $task
     * @param $state
     * @param $projectId
     * @internal param Project $project
     */
    public function __construct($task, $state, $projectId)
    {
        $this->task = $task;
        $this->state = $state;
        $this->projectId = $projectId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if($this->state == 2)
            $users = $this->task->user;
        else
            $users = Project::find($this->task->project_id)->users;

        Notification::send($users,
            new StateChangeTaskNotification($this->task, $this->state, $this->projectId));
    }
}