<?php

namespace App\Modules\Project\Jobs;

use App\Modules\Project\Models\Project;
use App\Modules\Project\Notifications\NewCommentNotification;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;

class NotifyAboutNewComment implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var User
     */
    protected $user;

    protected $project;

    /**
     * @param User $user
     * @param Project $project
     */
    public function __construct(User $user, Project $project)
    {
        $this->user = $user;
        $this->project = $project;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->user->hasRole('student')) {
            $users = User::find([$this->user->opponent_id, $this->user->manager_id]);
        } else {
            $users = User::where(['project_id' => $this->project->id])->get();
        }

        Notification::send($users,
            new NewCommentNotification([
                'title' => 'Nový komentář',
                'body' => 'Na projektu ' . $this->project->name . ' přibyl nový komentář',
                'type' => 'info', 'url' => route('project.show', $this->project->id)],
                $this->project));
    }
}
