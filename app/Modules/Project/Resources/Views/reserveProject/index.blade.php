@extends('master')

@section('content')
    <div class="row">
        @foreach($projects as $project)
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h4 class="box-title">{{ $project->name }}</h4>
                        <span class="text-muted pull-right">{{ $project->owner->name }}</span>
                    </div>
                    <div class="box-body">
                        {!! $project->task !!}
                    </div>
                    <div class="box-footer">
                        Počet volných míst: {{ $project->max_users - $project->users->count() }}
                        @if(Auth::user()->hasRole('student'))
                        <a href="{{ route('project.reserve', $project->id) }}"
                           class="btn btn-primary btn-sm pull-right">Zarezervovat projekt</a>
                        @endif
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection