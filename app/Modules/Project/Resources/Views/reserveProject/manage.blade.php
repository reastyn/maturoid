@extends('master')

@section('content')
    <div class="row">
        @forelse($reserve as $reserved)
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h4 class="box-title">
                            <a href="{{ route('project.show', $reserved->project->id) }}">
                                Projekt: {{ $reserved->project->name }}</a>
                        </h4>
                    </div>
                    <div class="box-body">
                        <div class="col-md-9">
                            <dl class="dl-horizontal">
                                <dt>Uživatel</dt>
                                <dd>{{ $reserved->user->name }}</dd>
                                <dt>Oponent uživatele</dt>
                                <dd>{{ $reserved->user->opponent ? $reserved->user->opponent->name : '' }}</dd>
                                <dt>Vedoucí uživatele</dt>
                                <dd>{{ $reserved->user->manager ? $reserved->user->opponent->manager->name : '' }}</dd>
                            </dl>
                        </div>
                        <div class="col-md-3">
                            <img width="60" height="60" class="img-circle" src="{{ $reserved->user->avatar_url }}" alt="">
                        </div>
                        <div class="text-center">
                            <a href="{{ route('reserveProject.deny', $reserved->id) }}"
                               class="btn btn-danger">Zamítnout rezervaci</a>
                            <a href="{{ route('reserveProject.approve', $reserved->id) }}"
                               class="btn btn-success">Schválit rezervaci</a>
                        </div>
                    </div>
                </div>
            </div>
        @empty
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="box">
                        <div class="box-body">
                                Žádné další rezervace nenalezeny.
                        </div>
                    </div>
                </div>
            </div>
        @endforelse
    </div>
@endsection