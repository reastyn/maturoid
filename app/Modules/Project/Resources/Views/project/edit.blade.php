@extends('master')

@section('content')
    @include('errors.alert')
    <form id="projectForm" method="POST"
          action="{{ route('project.update', $project->id) }}">
        {{ method_field('PUT') }}
        {{ csrf_field() }}
        <div class="project">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#general" data-toggle="tab">Obecné</a></li>
                <li><a href="#introduction" data-toggle="tab">Úvod</a></li>
                <li><a href="#anotation" data-toggle="tab">Anotace</a></li>
                <li><a href="#task" data-toggle="tab">Zadání, poznámka</a></li>
                <li><a href="#files" data-toggle="tab">Soubory</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="general">
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label for="name">Název práce</label>
                            <input type="text" class="form-control" name="name" id="name" value="{{ $project->name }}">
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="name_en">Název práce anglicky</label>
                            <input type="text" class="form-control" name="name_en" id="name_en"
                                   value="{{ $project->name_en }}">
                        </div>
                    </div>
                    @if(Auth::user()->hasRole(['admin', 'manager', 'opponent']))
                        <div class="row">
                            <div class="col-md-6 form-group no-help-block">
                                <label for="user_id">Uživatelé přidělení na projektu</label>
                                <select name="users[]" id="user_id" multiple>
                                    @foreach(\App\User::getAllWithRole('student') as $d)
                                        <option @if($project->users->contains($d)) selected @endif
                                            value="{{ $d->id }}">{{ $d->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6 form-group no-help-block">
                                <label for="state">Stav projektu</label>
                                <select name="state" id="state">
                                    @foreach(App\Modules\Project\Models\Project::$state as $k => $d)
                                        <option @if($d == $project->state) selected @endif value="{{ $k }}">{{ $d }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @push('scripts')
                            <script>
                                $('#user_id').select2({
                                    placeholder: 'Uživatelé přidělení na projektu',
                                    width: '100%',
                                    maximumSelectionLength: '{{ $project->max_users }}'
                                });
                                $('#state').select2({
                                    placeholder: 'Fáze projektu',
                                    width: '100%'
                                });
                            </script>
                            @endpush
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label for="max_users">Maximální počet žáků pracujících na projektu</label>
                            <input id="max_users" type="number" class="form-control" name="max_users"
                                   value="{{ $project->max_users }}">
                        </div>
                        <div class="col-md-6 form-group no-help-block">
                            <label for="category_id">Zaměření</label>
                            <select name="category_id" id="category_id">
                                @include('admin::user.options', ['data' => $categories, 'selected' => $project->category_id])
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label>Projekt</label>
                            <div id="project" class="dropzone"></div>
                        </div>
                    </div>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="introduction">
                    <label for="introduction">Úvod </label>
                    <textarea name="introduction" id="introduction"
                              class="editor">{{ $project->introduction }}</textarea>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="anotation">
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label for="annotation">Anotace </label>
                            <textarea name="annotation" id="annotation"
                                      class="editor">{{ $project->annotation }}</textarea>
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="annotation_en">Anotace (anglicky) </label>
                            <textarea name="annotation_en" id="annotation_en"
                                      class="editor">{{ $project->annotation_en }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="task">
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label for="task">Zadání práce </label>
                            <textarea name="task" id="task" class="editor">{{ $project->task }}</textarea>
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="note">Poznámka </label>
                            <textarea name="note" id="note"
                                      class="editor">{{ $project->note }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="files">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Materiály k projektu</label>
                            <div id="project-files" class="dropzone"></div>
                        </div>
                        <div class="col-md-6">
                            <label>Galerie</label>
                            <div id="gallery" class="dropzone"></div>
                            <p>Alespoň jeden screenshot z aplikace / fotku projektu. Hlavní fotku pojmenujte jako "projekt.formát"</p>
                        </div>
                        @hasrole(['admin', 'manager', 'opponent'])
                        <div class="col-md-12">
                            <label>Posudky</label>
                            <div id="report" class="dropzone"></div>
                        </div>
                        @push('scripts')
                        <script>
                            $(document).ready(function() {
                                initDropzone('#report', {
                                    maxFilesize: 250,
                                    url: '{{ route('project.upload', ['projekt' => $project->id, 'type' => 2]) }}',
                                    dictDefaultMessage: 'Soubor s posudkem o projektu',
                                    dictFileTooBig: 'Soubor má více než 250 MB',
                                    initUrl: '{{ route('project.getUploads', ['projekt' => $project->id, 'type' => 2]) }}',
                                    destroyUrl: '{{ route('project.destroyUpload', ['projekt' => $project->id, 'type' => 2]) }}'
                                });
                            });
                        </script>
                        @endpush
                        @endhasrole
                    </div>
                </div>
                <!-- /.tab-pane -->
                <input type="submit" name="submit" class="btn btn-primary pull-right" value="Uložit">
                <a href="{{ route('project.show', $project->id) }}" class="btn btn-default pull-right">Zpět</a>
            </div>
            <!-- /.tab-content -->
        </div>
    </form>
@stop

@push('scripts')
@include('vendor.tinymce.init', ['height' => '400'])
<style>
    .project {
        padding-bottom: 35px;
    }

    .project .mce-tinymce {
        margin-bottom: 10px;
    }

    .form-group {
        margin-bottom: 0;
    }

    .btn-primary.pull-right {
        margin-left: 5px;
    }
    
    #mceu_167 {
        display: none;
    }
</style>
<script type="text/javascript" src="{{ asset('js/upload.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! JsValidator::formRequest('App\Modules\Project\Http\Requests\ProjectRequest', '#projectForm') !!}
<script type="text/javascript">
    Dropzone.autoDiscover = false;
    var notification;

    function getFilesForDropzone(url, myDropzone, isGallery) {
        var thumbnailImage = isGallery || false;
        $.get(url, function (data) {
            var count = data.files.length;
            $.each(data.files, function (key, value) {
                var file = {name: value.original, size: value.size, unique: value.server};
                myDropzone.options.addedfile.call(myDropzone, file);
                myDropzone.emit("complete", file);
                if(thumbnailImage) {
                    myDropzone.createThumbnailFromUrl(file, value.url)
                }
            });
        });
    }

    function initDropzone(element, conf) {
        $(element).dropzone({
                url: conf.url,
                uploadMultiple: false,
                parallelUploads: 1,
                maxFilesize: conf.maxFilesize,
                addRemoveLinks: true,
                dictRemoveFile: 'Smazat',
                dictDefaultMessage: conf.dictDefaultMessage,
                dictFileTooBig: conf.dictFileTooBig,

                // The setting up of the dropzone
                init: function () {
                    getFilesForDropzone(conf.initUrl, this, conf.isGallery);

                    this.on('success', function () {
                        if(conf.successReload) {
                            $('#project').find('.dz-preview').hide('slow');
                            getFilesForDropzone(conf.initUrl, this);
                        }

                        if (typeof notification == undefined) {
                            notification = maturoid.notify('Hotovo', 'Soubor byly nahrán.', 'success');
                        }
                    });

                    this.removeAllFiles();
                },
                error: function (file, response) {
                    if ($.type(response) === "string")
                        var message = response;
                    else
                        var message = response.message;
                    file.previewElement.classList.add("dz-error");
                    var element = $(file.previewElement);
                    element.find('.dz-remove').hide();
                    setTimeout(function () {
                        element.hide('slow');
                    }, 10000);
                    _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
                    _results = [];
                    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                        node = _ref[_i];
                        _results.push(node.textContent = message);
                    }
                    return _results;
                },
                removedfile: function (file) {
                    $.ajax({
                        type: 'POST',
                        url: conf.destroyUrl,
                        data: {id: file.unique, _token: $('meta[name="csrf-token"]').attr('content')},
                        dataType: 'html',
                        success: function (data) {
                            maturoid.notify('Smazáno', 'Soubor byl úspěšně smazán.', 'success');
                            $(file.previewElement).hide('slow');
                        }
                    });
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            }
        );
    }

    initDropzone('#project-files', {
        maxFilesize: 8,
        url: '{{ route('project.upload', ['projekt' => $project->id]) }}',
        dictDefaultMessage: 'Sem přesuňte materiály k projektu',
        dictFileTooBig: 'Soubor má více než 8 MB',
        initUrl: '{{ route('project.getUploads', $project->id) }}',
        destroyUrl: '{{ route('project.destroyUpload', ['projekt' => $project->id]) }}'
    });

    initDropzone('#project', {
        maxFilesize: 250,
        url: '{{ route('project.uploadProject', ['projekt' => $project->id, 'type' => 3]) }}',
        dictDefaultMessage: 'Sem přesuňte soubor s projektem.',
        dictFileTooBig: 'Soubor má více než 250 MB',
        initUrl: '{{ route('project.getUploads', ['projekt' => $project->id, 'type' => 3]) }}',
        destroyUrl: '{{ route('project.destroyUpload', ['projekt' => $project->id, 'type' => 3]) }}',
        successReload: true
    });

    initDropzone('#gallery', {
        maxFilesize: 3,
        url: '{{ route('project.upload', ['projekt' => $project->id, 'type' => 1]) }}',
        dictDefaultMessage: 'Sem přesuňte obrázky projektu',
        dictFileTooBig: 'Soubor má více než 3 MB',
        initUrl: '{{ route('project.getUploads', ['projekt' => $project->id, 'type' => 1]) }}',
        destroyUrl: '{{ route('project.destroyUpload', ['projekt' => $project->id, 'type' => 1]) }}',
        isGallery: true
    });

    $(document).ready(function () {
        var title = $('.content-header h1, .breadcrumb .active a');
        $('#name').keyup(function () {
            title.text(this.value);
        });
        var categories = $('#category_id');
        categories.select2({
            placeholder: 'Vyberte zaměření projektu',
            width: '100%'
        });
        categories.on("select2:select", function (e) {
            categories.trigger('blur');
        });
        categories.trigger('blur');
    });
</script>
@endpush