@extends('master')

@section('content')
    <div class="row">
        <div class="col-md-3">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-upload"></i>

                    <h3 class="box-title">O projektu</h3>
                </div>
                <div class="box-body">
                    <dl>
                        <dt>Název projektu</dt>
                        <dd>{{ $project->name }}</dd>
                        <dt>Název projektu (anglicky)</dt>
                        <dd>{{ $project->name_en }}</dd>
                        <dt>Přidělení na projektu</dt>
                        <dd>{{ $project->users->implode('name', ', ') }}</dd>
                        <dt>Maximální počet žáků</dt>
                        <dd>{{ $project->max_users }}</dd>
                        <dt>Datum vytvoření</dt>
                        <dd>{{ $project->created_at->format('Y-m-d H:i') }}</dd>
                        <dt>Datum poslední úpravy</dt>
                        <dd>{{ $project->updated_at->format('Y-m-d H:i') }}</dd>
                        <dt>Zaměření</dt>
                        <dd>{{ $project->category->name }}</dd>
                        <dt>Stav</dt>
                        <dd>{{ $project->state }}</dd>
                        @if($projectFile)
                            <dt>Soubor s projektem</dt>
                            <dd><a href="{{ URL::to('storage/project') . '/' . $projectFile->filename }}" download>{{ $projectFile->filename }}</a></dd>
                        @endif
                        @if(Request::get('canEdit'))
                            <hr>
                            <a href="{{ route('project.edit', $project->id) }}"
                               class="btn btn-primary pull-right space-left">Upravit</a>
                            @if(Auth::user()->hasRole(['opponent', 'manager', 'admin']) && $project->getOriginal('state') == 0)
                                <a href="{{ route('project.approve', $project->id) }}"
                                   class="btn btn-success pull-right">
                                    Schválit projekt</a>
                            @endif
                        @endif
                    </dl>
                </div>
            </div>
        </div>
        <div class="col-md-9" id="project">
            <div class="project">
                <ul class="nav nav-tabs">
                    @if($project->task)
                        <li><a href="#task" data-toggle="tab">Zadání</a></li>
                    @endif
                    @if($project->introduction)
                        <li><a href="#introduction" data-toggle="tab">Úvod</a></li>
                    @endif
                    @if($project->annotation || $project->annotation_en)
                        <li><a href="#anotation" data-toggle="tab">Anotace</a></li>
                    @endif
                    <li><a href="#tasks" data-toggle="tab">Úkoly k projektu</a></li>
                    <li><a href="#files" data-toggle="tab">Soubory</a></li>
                    <li><a href="#gallery" data-toggle="tab">Galerie</a></li>
                    @if($project->note)
                        <li><a href="#comment" data-toggle="tab">Poznámka</a></li>
                    @endif
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="task">
                        {!! $project->task !!}
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="introduction">
                        {!! $project->introduction !!}
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="anotation">
                        <div class="row">
                            @if(empty($project->annotation_en))
                                <div class="col-md-12">
                                    {!! $project->annotation !!}
                                </div>
                            @else
                                <div class="col-md-6">
                                    {!! $project->annotation !!}
                                </div>
                                <div class="col-md-6">
                                    {!! $project->annotation_en !!}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="tab-pane" id="comment">
                        {!! $project->note !!}
                    </div>

                    <div class="tab-pane" id="tasks">
                        <div class="table-responsive">
                            <table class="table dataTable">
                                <thead>
                                <th></th>
                                <th></th>
                                <th>Úkol</th>
                                <th>Termín</th>
                                <th>Zadavatel</th>
                                <th style="width: 80px;">Akce</th>
                                </thead>
                                <tbody>
                                <tr v-for="task in tasks" v-bind:class="[task.state.done ? activeClass : '']">
                                    <td>
                                        <input type="checkbox" v-on:click="changeTaskState(task, 'done')"
                                         :checked="task.state.done">
                                    </td>
                                    <td v-bind:class="task.state.class">
                                        <span data-toggle="tooltip" v-bind:title="task.state.name" v-bind:data-original-title="task.state.name">
                                            <i class="fa" v-bind:class="task.state.icon"></i>
                                        </span>
                                    </td>
                                    <td>
                                        <input v-if="task.edit" :value="task.body" type="text" v-model="task.body"
                                               class="form-control input-sm" @keyup.enter="editTask(task, $event)"></input>
                                        <span v-else>@{{ task.body }}</span>
                                    </td>
                                    <td>
                                        <input v-if="task.edit" :value="task.due" type="text" v-model="task.due"
                                               class="form-control input-sm datepicker" data-provide="datepicker"
                                               @keyup.enter="editTask(task, $event)"></input>
                                        <span v-else>@{{ task.due }}</span>
                                    </td>
                                    <td>@{{ task.user.name }}</td>
                                    <td>
                                        <a v-if="userId == task.user.id" v-on:click="toggleEditTask(task, $event)" class="btn btn-primary btn-xs"
                                           data-toggle="tooltip" title="Upravit">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        <a v-if="userCanEdit" v-on:click="changeTaskState(task, 'approve')" class="btn btn-success btn-xs"
                                           data-toggle="tooltip" title="Úkol dokončen">
                                            <i class="fa fa-check"></i>
                                        </a>
                                        <a v-on:click="changeTaskState(task, 'urgent')" class="btn btn-warning btn-xs"
                                           data-toggle="tooltip" title="Urgovat">
                                            <i class="fa fa-exclamation-circle"></i>
                                        </a>
                                        <a v-on:click="deleteTask(task)"
                                           v-if="userId == task.user.id || userCanEdit || task.state.name == 'Dokončen'" class="btn btn-danger btn-xs"
                                           data-toggle="tooltip" title="Smazat">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <hr>
                        <form class="form-horizontal my-form" autocomplete="off" method="post" id="taskForm">
                            <div class="form-group">
                                <div class="col-md-6">
                                    <input name="body" type="text" placeholder="Zadejte úkol"
                                           class="form-control input-sm">
                                </div>
                                <div class="col-md-3">
                                    <input name="due" type="text" placeholder="Termín"
                                           class="form-control input-sm datepicker" data-provide="datepicker">
                                </div>
                                <div class="col-md-3">
                                    <button type="submit" class="btn btn-sm btn-primary btn-block">Přidat úkol</button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="tab-pane" id="files">
                        <label>Materiály k projektu</label>
                        @if($files->isEmpty())
                            <div class="alert alert-info">
                                Uživatel ještě nepřidal žádné soubory.
                            </div>
                        @else
                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <th>Soubor</th>
                                    <th>Velikost</th>
                                    <th style="width: 200px;">Akce</th>
                                </tr>
                                @foreach($files as $file)
                                    <tr>
                                        <td>{{ $file->original_name }}</td>
                                        <td>{{ \App\Helpers\Utils::bytesToHuman($file->size) }}</td>
                                        <td>
                                            <a href="{{ URL::to('storage/project') . '/' . $file->filename }}"
                                               class="btn btn-sm btn-default">Prohlédnout</a>
                                            <a href="{{ URL::to('storage/project') . '/' . $file->filename }}"
                                               class="btn btn-sm btn-primary" download>Stáhnout</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif
                        @if(!$reportFiles->isEmpty())
                            <label>Posudky</label>
                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <th>Soubor</th>
                                    <th>Velikost</th>
                                    <th style="width: 200px;">Akce</th>
                                </tr>
                                @foreach($reportFiles as $file)
                                    <tr>
                                        <td>{{ $file->original_name }}</td>
                                        <td>{{ \App\Helpers\Utils::bytesToHuman($file->size) }}</td>
                                        <td>
                                            <a href="{{ URL::to('storage/project') . '/' . $file->filename }}"
                                               class="btn btn-sm btn-default">Prohlédnout</a>
                                            <a href="{{ URL::to('storage/project') . '/' . $file->filename }}"
                                               class="btn btn-sm btn-primary" download>Stáhnout</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                    <div class="tab-pane" id="gallery">
                        <label>Galerie</label>
                        <div class="row">
                            @forelse($project->images as $image)
                                <div class="col-md-3">
                                    <a data-lightbox="project" href="{{ $imageUrl . $image->filename}}">
                                        <img src="{{ $imageUrl . $image->filename }}" class="img-responsive">
                                    </a>
                                </div>
                            @empty
                                <div class="col-md-12">
                                    <div class="alert alert-info">
                                        Žádné obrázky zatím nepřidány..
                                    </div>
                                </div>
                            @endforelse
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <div class="box box-widget">
                <div class="box-header with-border">
                    <h4 class="box-title">Komentáře</h4>
                </div>
                <div class="box-footer box-comments">
                    <span v-if="comments.length < 1">Žádné komentáře nenalezeny</span>
                    <div class="box-comment" v-for="comment in comments">
                        <img :src="comment.user.avatar_url" alt="" class="img-circle img-sm">
                        <div class="comment-text">
                            <span class="username">
                                @{{ comment.user.name }}
                                <span class="text-muted pull-right">
                                    <i class="fa fa-clock-o"></i>
                                    @{{ comment.human_created_at }}
                                    <span v-show="userId == comment.user.id || userCanEdit">
                                        |
                                        <i class="fa fa-pencil" v-show="!userCanEdit || userId == comment.user.id"
                                           v-on:click="toggleEditComment(comment)"></i>
                                        <i class="fa fa-trash" v-on:click="deleteComment(comment)"></i>
                                    </span>
                                </span>
                            </span>
                            <input v-if="comment.edit" :value="comment.body" type="text" v-model="comment.body"
                                   class="form-control input-sm" @keyup.enter="editComment(comment)"></input>
                            <span v-else>@{{ comment.body }}</span>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <form id="commentForm" method="post" autocomplete="off">
                        <img src="{{ Auth::user()->getAvatar() }}" alt="" class="img-responsive img-circle img-sm">
                        <div class="img-push">
                            <input name="body" class="form-control input-sm" type="text"
                                   placeholder="Odešlete pomocí enteru">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@push('scripts')
<script>
    $(document).ready(function () {
        var hash = window.location.hash;
        hash && $('ul.nav a[href="' + hash + '"]').tab('show');
        $('.nav-tabs').children().first().addClass('active');

        $('.nav-tabs a').click(function (e) {
            $(this).tab('show');
            var scrollmem = $('body').scrollTop() || $('html').scrollTop();
            window.location.hash = this.hash;
            $('html,body').scrollTop(scrollmem);
        });

        $.fn.datepicker.defaults.language = 'cs';
        $.fn.datepicker.defaults.format = "dd.mm.yyyy";

        $('#commentForm').on('submit', function (event) {
            event.preventDefault();
            var that = this;
            var data = $(this).serialize();
            if (data == 'body=') return;
            $.ajax({
                type: "POST",
                url: '{!! route('project.comment.store', $project->id) !!}',
                data: data,
                success: function () {
                    project.loadComments();
                    that.reset();
                },
                error: function () {
                    maturoid.notify('Komentář se nepodařilo přidat.', 'Zkuste to prosím později', 'error');
                }
            });
        });

        $('#taskForm').on('submit', function (event) {
            event.preventDefault();
            var that = this;
            var data = $(this).serializeArray();
            if(data['body'] == '') return;
            $.ajax({
                type: "POST",
                url: '{!! route('project.task.store', $project->id) !!}',
                data: data,
                success: function () {
                    project.loadTasks();
                    that.reset();
                },
                error: function () {
                    maturoid.notify('Komentář se nepodařilo přidat.', 'Zkuste to prosím později', 'error');
                }
            });
        })
    });

    maturoid.echo.private('project.{{ $project->id }}')
        .listen('.App.Modules.Project.Events.CommentChange', function() {
            if(inEditComments.length > 0)
                setTimeout(function() {
                    project.loadComments();
                }, 3000);
            else
                project.loadComments();
        })
        .listen('.App.Modules.Project.Events.TaskChange', function() {
            if(inEditTasks.length > 0)
                setTimeout(function() {
                    project.loadTasks();
                }, 3000);
            else
                project.loadTasks();
        });

    var inEditComments = [];
    var inEditTasks = [];

    var project = new Vue({
        el: '#project',
        data: {
            comments: [],
            tasks: [],
            activeClass: 'bg-gray',
            userId: $('#user-id').attr('content'),
            userCanEdit: @if(Auth::user()->hasRole(['manager', 'opponent', 'admin']))true @else false @endif
        },
        methods: {
            loadComments: function () {
                var that = this;

                $.get('{{ route('project.comments', $project->id) }}', function (data) {
                    that.comments = data;
                });
            },
            deleteFromArray: function(array, element) {
                for(var i = array.length - 1; i >= 0; i--) {
                    if(array[i] == element) {
                        array.splice(i, 1);
                    }
                }
            },
            toggleEditComment: function (comment, e) {
                if(comment.edit) {
                    this.editComment(comment, e);
                }
                this.$set(comment, 'edit', !comment.edit);
                if(comment.edit) {
                    inEditComments.push(comment.id);
                } else {
                    this.deleteFromArray(inEditComments, comment.id);
                }
            },
            toggleEditTask: function(task, e) {
                if(task.edit) {
                    this.editTask(task, e);
                }
                this.$set(task, 'edit', !task.edit);
                if(task.edit) {
                    inEditTasks.push(task.id);
                } else {
                    this.deleteFromArray(inEditTasks, task.id);
                }
            },
            loadTasks: function() {
                var that = this;

                $.get('{{ route('project.tasks', $project->id) }}', function (data) {
                    that.tasks = data;
                });
            },
            changeTaskState: function(task, type) {
                var state;
                switch(type) {
                    case 'approve':
                        state = 3;
                        break;
                    case 'urgent':
                        state = 0;
                        if(maturoid.userCanEdit)
                            maturoid.notify('Úkol byl urgován', 'Studentovi byl odeslán email a notifikace.', 'success');
                        break;
                    default:
                        state = !task.state.done ? 2 : 1;
                        break;
                }

                var that = this;
                $.ajax({
                    type: "POST",
                    url: '{!! route('project.task.state', $project->id) !!}',
                    data: {
                        task_id: task.id,
                        state: state
                    },
                    success: function () {
                        project.loadTasks();
                    },
                    error: function () {
                        maturoid.notify('Úkol se nepodařilo upravit..', 'Zkuste to prosím později', 'error');
                    }
                });
            },
            editTask: function (task, e) {
                var target = $(e.target);
                $('.datepicker-dropdown').remove();
                if(target.hasClass('datepicker')) {
                    task.due = target.val();
                } else {
                    task.due = target.closest('tr').children().eq(3).children().val();
                }
                var that = this;
                $.ajax({
                    type: "POST",
                    url: '{!! route('project.task.store', $project->id) !!}',
                    data: {
                        body: task.body,
                        task_id: task.id,
                        due: task.due
                    },
                    success: function () {
                        that.$set(task, 'edit', false);
                        that.deleteFromArray(inEditTasks, task.id);
                    },
                    error: function () {
                        maturoid.notify('Úkol se nepodařilo upravit..', 'Zkuste to prosím později', 'error');
                    }
                });
            },
            deleteTask: function (task) {
                if (confirm('Opravdu chcete úkol smazat?')) {
                    $.ajax({
                        type: "POST",
                        url: '{!! route('project.task.delete', $project->id) !!}',
                        data: {
                            task_id: task.id
                        },
                        success: function () {
                            project.loadTasks();
                            maturoid.notify('Smazáno', 'Úkol byl úspěšně smazán.', 'success');
                        },
                        error: function () {
                            maturoid.notify('Úkol se nepodařilo smazat.', 'Zkuste to prosím později', 'error');
                        }
                    });
                }
            },
            editComment: function (comment) {
                var that = this;
                $.ajax({
                    type: "POST",
                    url: '{!! route('project.comment.store', $project->id) !!}',
                    data: {
                        body: comment.body,
                        comment_id: comment.id
                    },
                    success: function () {
                        that.$set(comment, 'edit', false);
                        that.deleteFromArray(inEditComments, comment.id);
                    },
                    error: function () {
                        maturoid.notify('Komentář se nepodařilo upravit..', 'Zkuste to prosím později', 'error');
                    }
                });
            },
            deleteComment: function (comment) {
                if (confirm('Opravdu chcete komentář smazat?')) {
                    $.ajax({
                        type: "POST",
                        url: '{!! route('project.comment.delete', $project->id) !!}',
                        data: {
                            comment_id: comment.id
                        },
                        success: function () {
                            project.loadComments();
                        },
                        error: function () {
                            maturoid.notify('Komentář se nepodařilo smazat.', 'Zkuste to prosím později', 'error');
                        }
                    });
                }
            }
        },
        mounted: function () {
            this.loadComments();
            this.loadTasks();
        },
    });
    lightbox.option({
        'albumLabel': 'Obrázek %1 z %2'
    })
</script>
<style>
    .my-form .form-group [class*="col-"] {
        padding-left: 3%;
        padding-right: 0;
    }

    .my-form .btn-block {
        max-width: 85%;
    }

    @media (max-width:991px) {
        .my-form .form-group [class*="col-"] {
            margin-bottom: 5px;
            max-width: 95%;
        }

        .my-form .btn-block {
            max-width: 100%;
        }
    }

    #tasks tr > th:first-child, th:nth-child(2) {
        text-align: center;
        padding: 0 0 8px;
    }

    #tasks tr > td:first-child, td:nth-child(2) {
        width: 25px;
        text-align: center;
        padding: 0;
    }

    #tasks tr > td:first-child > input {
        margin-top: 12px;
    }
</style>
@endpush