@extends('master')

@section('content')
    @include('errors.alert')
    <div class="box box-primary">
        <div class="box-body">
            <div class="progress">
                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemin="0"
                     aria-valuemax="100"></div>
            </div>
            <form id="projectForm" method="POST"
                  action="{{ route('makeProject.store') }}">
                {{ csrf_field() }}
                <fieldset>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label for="name">Název práce</label>
                            <input type="text" class="form-control" name="name" id="name">
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="name_en">Název práce anglicky</label>
                            <input type="text" class="form-control" name="name_en" id="name_en">
                        </div>
                        </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label for="max_users">Maximální počet žáků pracujících na projektu</label>
                            <input type="number" id="max_users" class="form-control" name="max_users" value="1">
                        </div>
                        <div class="col-md-6 form-group no-help-block">
                            <label for="category_id">Zaměření </label>
                            <select name="category_id" id="category_id">
                                @include('admin::user.options', ['data' => $categories])
                            </select>
                        </div>
                    </div>
                    <input type="button" name="password" class="next btn btn-primary pull-right" value="Pokračovat">
                </fieldset>
                <fieldset>
                    <div class="row form-group">
                        <div class="col-md-6">
                            <label for="introduction">Úvod </label>
                            <textarea name="introduction" id="introduction" class="editor"></textarea>
                        </div>
                        <div class="col-md-6">
                            <label for="task">Zadání práce </label>
                            <textarea name="task" id="task" class="editor"></textarea>
                        </div>
                    </div>
                    <input type="button" name="next" class="next btn btn-primary pull-right" value="Pokračovat">
                    <input type="button" name="previous" class="previous btn btn-default pull-right" value="Předchozí">
                </fieldset>
                <fieldset>
                    <div class="row form-group">
                        <div class="col-md-6">
                            <label for="annotation">Anotace </label>
                            <textarea name="annotation" id="task" class="editor"></textarea>
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="note">Poznámka </label>
                            <textarea name="note" id="note" class="editor"></textarea>
                        </div>
                    </div>
                    <input type="submit" name="submit" class="submit btn btn-success pull-right" value="Vytvořit">
                    <input type="button" name="previous" class="previous btn btn-default pull-right" value="Předchozí">
                </fieldset>
                {{--<button type="submit" class="btn btn-primary pull-right">Další</button>--}}
            </form>
        </div>
    </div>
@endsection

@push('scripts')
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! JsValidator::formRequest('App\Modules\Project\Http\Requests\ProjectRequest', '#projectForm') !!}
@include('vendor.tinymce.init', ['height' => '400'])
<style>
    #projectForm fieldset:not(:first-of-type) {
        display: none;
    }
    input[type=button] {
        margin-right: 5px;
    }
</style>
<script>
    $(document).ready(function () {
        var categories = $('#category_id');
        categories.select2({
            placeholder: 'Vyberte zaměření projektu',
            width: '100%'
        });
        categories.on("select2:select", function (e) {
            categories.trigger('blur');
        });
        var enName = $('#name_en');
        $('#name').keyup(function () {
            enName.val(this.value);
            enName.valid();
            $(this).valid();
        });
        var current = 1, current_step, next_step, steps;
        steps = $("fieldset").length;
        $(".next").click(function () {
            current_step = $(this).parent();
            next_step = $(this).parent().next();
            next_step.show();
            current_step.hide();
            setProgressBar(++current);
        });
        $(".previous").click(function () {
            current_step = $(this).parent();
            next_step = $(this).parent().prev();
            next_step.show();
            current_step.hide();
            setProgressBar(--current);
        });
        setProgressBar(current);
        // Change progress bar action
        function setProgressBar(curStep) {
            var percent = parseFloat(100 / steps) * curStep;
            percent = percent.toFixed();
            $(".progress-bar")
                    .css("width", percent + "%")
                    .html(percent + "%");
        }
    });
</script>
@endpush