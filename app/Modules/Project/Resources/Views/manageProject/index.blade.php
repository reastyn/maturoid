@extends('master')

@section('content')
    <div class="row">
        <div class="col-md-3">
            <div class="box box-primary">
                <div class="box-header with-border"><h3 class="box-title">Filtr</h3></div>
                <div class="box-body container-fluid">
                    <div class="form-group">
                        <label for="state">Stav projektu</label>
                        <select name="state" id="state">
                            <option></option>
                            @foreach($states as $key => $state)
                                <option value="{{ $key }}">{{ $state }}</option>
                            @endforeach
                        </select>

                        <label for="category">Kategorie projektu</label>
                        <select name="category" id="category">
                            <option></option>
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="box">
                <div class="box-body">
                    <table class="table table-bordered dataTable" id="users-table">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Název projektu</th>
                            <th>Maximální počet už.</th>
                            <th>Přidělení studenti</th>
                            <th>Stav</th>
                            <th style="width: 195px">Akce</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@push('scripts')
<script>
    $(function () {
        $('#state').on('change', function () {
            dataTable.ajax.reload();
        }).select2({
            placeholder: 'Vyberte stav projektu',
            width: '100%',
            allowClear: true
        });

        $('#category').on('change', function () {
            dataTable.ajax.reload();
        }).select2({
            placeholder: 'Vyberte stav projektu',
            width: '100%',
            allowClear: true
        });

        // this is the id of the form
        $(".assignForm").submit(function (e) {
            $.ajax({
                type: "POST",
                url: $(this).attr('action'),
                data: {
                    assign: $(this).serializeArray(),
                    selected: selected
                },
                success: function() {
                    maturoid.notify('Úspěch', 'Vedoucí / oponenti byli úspěšně přiděleni uživatelům', 'success');
                },
                error: function() {
                    maturoid.notify('Něco se pokazilo..', 'Nevybrali jste vedoucího / oponenta', 'error');
                },
                complete: function () {
                    dataTable.ajax.reload();
                }
            });
            e.preventDefault();
        });

        var table = $('#users-table');
        var selected = [];
        table.on('click', '.deleteProject', function () {
            if (confirm('Opravdu projekt smazat?')) {
                $.ajax({
                    type: "POST",
                    url: '{!! route('project.destroy') !!}',
                    data: {
                        projekt: $(this).data('id')
                    },
                    complete: function () {
                        dataTable.ajax.reload();
                    }
                });
            }
        });
        table.on('change', 'input[type=checkbox]', function () {
            var id = parseInt(this.value);
            var index = $.inArray(id, selected);

            if (index === -1) {
                selected.push(id);
            } else {
                selected.splice(index, 1);
            }

            if (selected.length > 0) {
                $('.must-check').removeClass('r-disable');
            } else {
                $('.must-check').addClass('r-disable');
            }

            $(this).closest('tr').toggleClass('selected');
        });
        var dataTable = table.DataTable({
            pageLength: 50,
            autoWidth: false,
            processing: true,
            serverSide: true,
            ajax: {
                url: '{!! route('manageProject.data') !!}',
                data: function(d) {
                    d.state = $('#state').val();
                    d.category = $('#category').val();
                }
            },
            order: [[1, 'asc']],
            "rowCallback": function (row, data) {
                if ($.inArray(data.DT_RowId, selected) !== -1) {
                    $(row).addClass('selected').find('input[type=checkbox]').attr('checked', true);
                }
            },
            columns: [
                {data: 'checkbox', name: 'checkbox', orderable: false, searchable: false, width: "1px"},
                {data: 'name', name: 'name'},
                {data: 'max_users', name: 'max_users'},
                {data: 'users[, ].name', name: 'users.name', searchable: true},
                {data: 'state', name: 'state'},
                {data: 'action', name: 'action', orderable: false, sortable: false, searchable: false},
            ]
        });
    });
</script>
@endpush