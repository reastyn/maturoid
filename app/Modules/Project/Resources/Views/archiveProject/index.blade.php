@extends('master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h4 class="box-title">Kategorie</h4>
                </div>
                <div class="box-body categories">
                    <a href="{{ route('archiveProject.index') }}">
                        <span class="label label-success">Všechny kategorie</span>
                    </a>
                    @foreach($categories as $cat)
                        <a href="{{ route('archiveProject.index', $cat->id) }}">
                            <span class="label @if($category && $category->id == $cat->id) label-primary @else label-default @endif">{{ $cat->name }}</span>
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="row projects">
        @forelse($projects as $project)
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h4 class="box-title">{{ $project->name }}</h4>
                        <span class="text-muted pull-right">{{ $project->users->implode('name', ', ') }}</span>
                    </div>
                    <div class="box-body">
                        {!! $project->task !!}
                        <img src="{{ Storage::url('project') . '/' . $project->printscreen->first()['filename'] }}" alt="">
                    </div>
                    <div class="box-footer">
                        Dokončeno: {{ $project->updated_at->format('d.m.Y') }}
                        <a href="{{ route('archiveProject.show', $project->id) }}"
                           class="btn btn-primary btn-sm pull-right">Zobrazit projekt</a>
                    </div>
                </div>
            </div>
        @empty
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="alert alert-info">
                        Další archivované projekty nenalezeny.
                    </div>
                </div>
            </div>
        @endforelse
    </div>
@endsection