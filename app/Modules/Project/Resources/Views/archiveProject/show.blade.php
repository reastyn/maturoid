@extends('master')

@section('content')
    <div class="row">
        <div class="col-md-3">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-upload"></i>

                    <h3 class="box-title">O projektu</h3>
                </div>
                <div class="box-body">
                    <dl>
                        <dt>Název projektu</dt>
                        <dd>{{ $project->name }}</dd>
                        <dt>Název projektu (anglicky)</dt>
                        <dd>{{ $project->name_en }}</dd>
                        <dt>Přidělení na projektu</dt>
                        <dd>{{ $project->users->implode('name', ', ') }}</dd>
                        <dt>Počet žáků</dt>
                        <dd>{{ $project->max_users }}</dd>
                        <dt>Datum vytvoření</dt>
                        <dd>{{ $project->created_at->format('Y-m-d H:i') }}</dd>
                        <dt>Datum poslední úpravy</dt>
                        <dd>{{ $project->updated_at->format('Y-m-d H:i') }}</dd>
                        <dt>Zaměření</dt>
                        <dd>{{ $project->category->name }}</dd>
                        <dt>Stav</dt>
                        <dd>{{ $project->state }}</dd>
                    </dl>
                </div>
            </div>
        </div>
        <div class="col-md-9" id="project">
            <div class="project">
                <ul class="nav nav-tabs">
                    @if($project->task)
                        <li><a href="#task" data-toggle="tab">Zadání</a></li>
                    @endif
                    @if($project->introduction)
                        <li><a href="#introduction" data-toggle="tab">Úvod</a></li>
                    @endif
                    @if($project->annotation || $project->annotation_en)
                        <li><a href="#anotation" data-toggle="tab">Anotace</a></li>
                    @endif
                    <li><a href="#files" data-toggle="tab">Soubory</a></li>
                    <li><a href="#gallery" data-toggle="tab">Galerie</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="task">
                        {!! $project->task !!}
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="introduction">
                        {!! $project->introduction !!}
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="anotation">
                        <div class="row">
                            @if(empty($project->annotation_en))
                                <div class="col-md-12">
                                    {!! $project->annotation !!}
                                </div>
                            @else
                                <div class="col-md-6">
                                    {!! $project->annotation !!}
                                </div>
                                <div class="col-md-6">
                                    {!! $project->annotation_en !!}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="tab-pane" id="files">
                        <label>Materiály k projektu</label>
                        @if($files->isEmpty())
                            <div class="alert alert-info">
                                Uživatel ještě nepřidal žádné soubory.
                            </div>
                        @else
                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <th>Soubor</th>
                                    <th>Velikost</th>
                                    <th style="width: 200px;">Akce</th>
                                </tr>
                                @foreach($files as $file)
                                    <tr>
                                        <td>{{ $file->original_name }}</td>
                                        <td>{{ \App\Helpers\Utils::bytesToHuman($file->size) }}</td>
                                        <td>
                                            <a href="{{ URL::to('storage/project') . '/' . $file->filename }}"
                                               class="btn btn-sm btn-default">Prohlédnout</a>
                                            <a href="{{ URL::to('storage/project') . '/' . $file->filename }}"
                                               class="btn btn-sm btn-primary" download>Stáhnout</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif
                        @if(!$reportFiles->isEmpty())
                            <label>Posudky</label>
                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <th>Soubor</th>
                                    <th>Velikost</th>
                                    <th style="width: 200px;">Akce</th>
                                </tr>
                                @foreach($reportFiles as $file)
                                    <tr>
                                        <td>{{ $file->original_name }}</td>
                                        <td>{{ \App\Helpers\Utils::bytesToHuman($file->size) }}</td>
                                        <td>
                                            <a href="{{ URL::to('storage/project') . '/' . $file->filename }}"
                                               class="btn btn-sm btn-default">Prohlédnout</a>
                                            <a href="{{ URL::to('storage/project') . '/' . $file->filename }}"
                                               class="btn btn-sm btn-primary" download>Stáhnout</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                    <div class="tab-pane" id="gallery">
                        <label>Galerie</label>
                        <div class="row">
                            @forelse($project->images as $image)
                                <div class="col-md-3">
                                    <a data-lightbox="project" href="{{ $imageUrl . $image->filename}}">
                                        <img src="{{ $imageUrl . $image->filename }}" class="img-responsive">
                                    </a>
                                </div>
                            @empty
                                <div class="col-md-12">
                                    <div class="alert alert-info">
                                        Žádné obrázky zatím nepřidány..
                                    </div>
                                </div>
                            @endforelse
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
        </div>
    </div>
@stop

@push('scripts')
<script>
    $(document).ready(function() {
        $('.nav-tabs').children().first().addClass('active');
        lightbox.option({
            'albumLabel': 'Obrázek %1 z %2'
        })
    });
</script>
@endpush