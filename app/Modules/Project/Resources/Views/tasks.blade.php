<ul class="menu">
    @forelse($tasks as $task)
        <li>
            <a href="{{ $route }}">
                <i class="fa {{$task->state['icon'] }} {{$task->state['class'] }}"></i> {{$task->body}}
                <span class="pull-right text-muted">{{ $task->due }}</span>
            </a>
        </li>
    @empty
        <li>
            <a href="#" class="text-center">
                Všechny úkoly vyřešeny
            </a>
        </li>
    @endforelse
</ul>