<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('task')->nullable();
            $table->string('annotation')->nullable();
            $table->string('annotation_en')->nullable();
            $table->string('name');
            $table->string('name_en');
            $table->string('introduction')->nullable();
            $table->string('analysis')->nullable();
            $table->integer('max_users');
            $table->integer('created_by')->unsigned();
            $table->smallInteger('state')->default(0);
            $table->integer('category_id')->unsigned()->nullable();
            $table->string('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('projects');
    }
}
