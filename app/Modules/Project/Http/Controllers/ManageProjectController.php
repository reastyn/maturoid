<?php

namespace App\Modules\Project\Http\Controllers;

use App\Modules\Project\Models\Category;
use App\Modules\Project\Models\Project;
use App\User;
use Datatables;
use Illuminate\Http\Request;

class ManageProjectController
{
    public function index()
    {
        return view('project::manageProject.index', ['states' => Project::$state, 'categories' => Category::all()]);
    }

    public function data(Request $request)
    {
        if($request->user()->hasRole('admin')) {
            $projects = Project::with('users');
        } else {
            $id = $request->user()->id;
            $projectIds = User::where('opponent_id', $id)->orWhere('manager_id', $id)->pluck('project_id')->toArray();
            $projects = Project::with('users')->whereIn('id', $projectIds)->orWhere('created_by', $id);
        }

        return Datatables::of($projects)
            ->setRowId('id')
            ->addColumn('action', function ($project) {
                return '<a href="' . route('project.show', $project->id) . '" class="btn btn-xs btn-default">
                <i class="fa fa-eye"></i> Zobrazit</a> 
                <a href="' . route('project.edit', $project->id) . '" class="btn btn-xs btn-primary">
                <i class="glyphicon glyphicon-edit"></i> Upravit</a>
                <a data-id="' . $project->id . '" href="#" class="btn btn-xs btn-danger deleteProject">
                <i class="glyphicon glyphicon-trash"></i> Smazat</a>';
            })
            ->filter(function ($query) use ($request) {
                if ($request->has('state')) {
                    $query->where('state', 'like', "%{$request->get('state')}%");
                }
                if ($request->has('category')) {
                    $query->where('category_id', 'like', "%{$request->get('category')}%");
                }
            }, true)
            ->addColumn('checkbox', function ($project) {
                return '<input type="checkbox" value="' . $project->id . '">';
            })
            ->make(true);
    }
}