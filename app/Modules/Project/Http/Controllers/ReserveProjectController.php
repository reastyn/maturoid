<?php

namespace App\Modules\Project\Http\Controllers;

use App\Modules\Project\Models\Project;
use App\Modules\Project\Models\ReservedProject;
use App\Modules\Project\Notifications\NewReserveProjectNotification;
use App\Modules\Project\Notifications\ReserveProjectApprovedNotification;
use App\Modules\Project\Notifications\ReserveProjectDenyNotification;
use App\User;
use Illuminate\Http\Request;

class ReserveProjectController
{

    public function index()
    {
        return view('project::reserveProject.index', ['projects' => Project::with('owner', 'users')->where(['state' => 2])->get()]);
    }

    public function manage(Request $request)
    {
        $projectIds = Project::where(['created_by' => $request->user()->id, 'state' => 2])->pluck('id')->toArray();
        $reservedProjects = ReservedProject::with('user', 'project')->whereIn('project_id', $projectIds)->get();
        return view('project::reserveProject.manage', ['reserve' => $reservedProjects]);
    }

    public function approve(Request $request, $reserved)
    {
        $reserved = ReservedProject::with('user')->find($reserved);
        $user = $reserved->user;
        if ($user->hasRole('student') && $user->project_id == null) {
            if($reserved->project->max_users <= User::where('project_id', $reserved->project->id)->count())
                return redirect()->route('reserveProject.manage')->with([
                    'title' => 'Rezervace nemůže být schválena',
                    'body' => 'Projekt má již příliš mnoho uživatelů',
                    'type' => 'danger'
                ]);
            $user->project_id = $reserved->project_id;
            $user->save();
            $user->notify(new ReserveProjectApprovedNotification($reserved));
            $reserved->delete();

            return redirect()->route('reserveProject.manage')->with([
                'title' => 'Úspěch',
                'body' => 'Uživatel byl přidělen na projekt.',
                'type' => 'success'
            ]);
        }
        return redirect()->route('reserveProject.manage')->with([
            'title' => 'Rezervace nemůže být schválena',
            'body' => 'Uživatel již má přidělený nějaký projekt',
            'type' => 'danger'
        ]);
    }

    public function deny(Request $request, $reserved)
    {
        $reserved = ReservedProject::with('user')->find($reserved);
        $reserved->user->notify(new ReserveProjectDenyNotification($reserved));
        $reserved->delete();

        return redirect()->route('reserveProject.manage')->with([
            'title' => 'Zamítnuto',
            'body' => 'Rezervace projektu byla uživateli zamítnuta.',
            'type' => 'success'
        ]);
    }

    public function reserve(Request $request, $project)
    {
        $reserved = ReservedProject::firstOrCreate([
            'project_id' => $project,
            'user_id' => $request->user()->id
        ]);

        Project::find($project)->owner->notify(new NewReserveProjectNotification($reserved, $request->user()));

        return redirect()->route('reserveProject.index')->with([
            'title' => 'Úspěch',
            'body' => 'Projekt byl úspěšně zarezervován.',
            'type' => 'success'
        ]);
    }
}