<?php

namespace App\Modules\Project\Http\Controllers;

use App\Modules\Project\Http\Requests\ProjectRequest;
use App\Modules\Project\Models\Category;
use App\Modules\Project\Models\Project;
use App\Modules\Project\Notifications\ProjectNotification;
use App\User;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mews\Purifier\Facades\Purifier;
use Notification;

class MakeProjectController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('project::makeProject.index', ['categories' => Category::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProjectRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectRequest $request)
    {
        $project = new Project;
        $project->name = $request->name;
        $project->name_en = $request->name_en;
        $project->max_users = $request->max_users;
        $project->category_id = $request->category_id;
        $project->task = Purifier::clean($request->task);
        $project->note = Purifier::clean($request->note);
        $project->annotation = Purifier::clean($request->annotation);
        $project->annotation_en = Purifier::clean($request->annotation_en);
        $project->introduction = Purifier::clean($request->introduction);
        $project->analysis = Purifier::clean($request->analysis);
        $project->created_by = $request->user()->id;

        $user = $request->user();
        if ($user->hasRole(['opponent', 'manager', 'admin'])) {
            $project->state = 3;
            $project->save();
            return redirect()->route('manageProject.index')->with([
                'title' => 'Úspěch',
                'body' => 'Projekt byl úspěšně založen.',
                'type' => 'success'
            ]);
        }
        $project->save();

        $user->update(['project_id' => $project->id]);

        Notification::send(User::find([$user->opponent_id, $user->manager_id]),
            new ProjectNotification([
                'title' => 'Uživatel ' . $user->name . ' založil nový projekt',
                'body' => 'Zkontrolujte ho prosím a popřípadě odsouhlaste',
                'type' => 'info', 'url' => route('project.show', $project->id)],
                $project, $user));

        return redirect()->route('project.my')->with([
            'title' => 'Úspěch',
            'body' => 'Projekt byl poslán ke schválení oponentovi a vedoucímu',
            'type' => 'success'
        ]);
    }
}
