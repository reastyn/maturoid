<?php

namespace App\Modules\Project\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Project\Models\Category;
use App\Modules\Project\Models\Project;
use App\Modules\Project\Models\ProjectFile;
use URL;

class ArchiveProjectController extends Controller
{
    public function index($category = null)
    {
        $category = Category::find($category);
        $categories = Category::all();
        $projects = Project::with('images', 'printscreen')->where('state', 4)->orderBy('updated_at', 'desc');
        if ($category)
            $projects = $projects->where('category_id', $category->id)->get();
        else
            $projects = $projects->get();


        return view('project::archiveProject.index', ['projects' => $projects, 'categories' => $categories, 'category' => $category]);
    }

    public function show($id)
    {
        $project = Project::with('category', 'users', 'images')->find($id);
        $files = ProjectFile::where(['project_id' => $id, 'type' => 0])->get();
        $report = ProjectFile::where(['project_id' => $id, 'type' => 2])->get();
        $projectFile = ProjectFile::where(['project_id' => $id, 'type' => 3])->orderBy('created_at')->first();
        return view('project::archiveProject.show', ['project' => $project, 'files' => $files,
            'custom' => ['href' => route('project.show', $project->id), 'breadcrumb' => $project->name],
            'reportFiles' => $report, 'projectFile' => $projectFile, 'imageUrl' => URL::to('storage/project') . '/']);
    }
}