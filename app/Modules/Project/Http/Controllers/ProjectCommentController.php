<?php

namespace App\Modules\Project\Http\Controllers;

use App\Modules\Project\Events\CommentChange;
use App\Modules\Project\Jobs\NotifyAboutNewComment;
use App\Modules\Project\Models\Project;
use App\Modules\Project\Models\ProjectComment;
use Illuminate\Http\Request;

class ProjectCommentController
{

    public function get($projectId)
    {
        return ProjectComment::with('user')->orderBy('created_at', 'asc')->where('project_id', $projectId)->get();
    }

    /**
     * @param Request $request
     * @param $id
     * @return ProjectComment
     */
    public function store(Request $request, $id)
    {
        if ($request->body == null) abort(400);
        if ($request->comment_id != null) {
            ProjectComment::where(['id' => $request->comment_id, 'project_id' => $id, 'user_id' => $request->user()->id])
                ->update(['body' => $request->body]);
            event(new CommentChange($id));
            return;
        }

        $project = Project::find($id);
        $user = $request->user();
        dispatch(new NotifyAboutNewComment($user, $project));

        ProjectComment::create([
            'project_id' => $id,
            'body' => $request->body,
            'user_id' => $request->user()->id
        ]);
        event(new CommentChange($id));
    }

    /**
     * @param Request $request
     * @param $id
     * @return ProjectComment | string
     */
    public function delete(Request $request, $id)
    {
        if ($request->user()->hasRole(['manager', 'opponent', 'admin'])) {
            ProjectComment::where(['project_id' => $id,
                'id' => $request->comment_id])->delete() ? '' : abort(400);
            event(new CommentChange($id));
            return;
        }
        ProjectComment::where(['project_id' => $id,
            'id' => $request->comment_id, 'user_id' => $request->user()->id])->delete() ? '' : abort(400);
        event(new CommentChange($id));
        return;
    }
}