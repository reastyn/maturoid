<?php

namespace App\Modules\Project\Http\Controllers;

use App\Modules\Project\Helpers\ProjectRepository;
use App\Modules\Project\Http\Requests\ProjectRequest;
use App\Modules\Project\Models\Category;
use App\Modules\Project\Models\Project;
use App\Modules\Project\Models\ProjectComment;
use App\Modules\Project\Models\ProjectFile;
use App\Modules\Project\Models\ProjectTask;
use App\Modules\Project\Models\ReservedProject;
use App\Modules\Project\Notifications\NewReportNotification;
use App\Modules\Project\Notifications\StateChangeProjectNotification;
use App\User;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mews\Purifier\Facades\Purifier;
use Notification;
use Response;
use URL;
use Validator;

class ProjectController extends Controller
{

    /**
     * @var ProjectRepository
     */
    protected $file;

    /**
     * ProjectController constructor.
     * @param ProjectRepository $projectRepository
     */
    public function __construct(ProjectRepository $projectRepository)
    {
        $this->file = $projectRepository;
        $this->middleware('canEditProject', ['except' => ['index', 'showMyProject']]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = Project::with('category', 'users')->find($id);
        $files = ProjectFile::where(['project_id' => $id, 'type' => 0])->get();
        $report = ProjectFile::where(['project_id' => $id, 'type' => 2])->get();
        $projectFile = ProjectFile::where(['project_id' => $id, 'type' => 3])->orderBy('created_at')->first();
        return view('project::project.show', ['project' => $project, 'files' => $files,
            'custom' => $this->makeBreadcrumb($project), 'reportFiles' => $report, 'projectFile' => $projectFile,
            'imageUrl' => URL::to('storage/project') . '/']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = Project::find($id);
        return view('project::project.edit', ['project' => $project, 'custom' => $this->makeBreadcrumb($project), 'categories' => Category::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProjectRequest|Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectRequest $request, $id)
    {
        $this->validate($request, [
            'category_id' => 'exists:categories,id'
        ], [
            'category_id' => 'Kategorie musí existovat!'
        ]);
        $project = Project::find($id);
        $project->name = $request->name;
        $project->name_en = $request->name_en;
        $project->max_users = $request->users ? count($request->users) : null;
        $project->max_users = $request->max_users;
        $project->category_id = $request->category_id;
        $project->task = Purifier::clean($request->task);
        $project->note = Purifier::clean($request->note);
        $project->annotation = Purifier::clean($request->annotation);
        $project->annotation_en = Purifier::clean($request->annotation_en);
        $project->introduction = Purifier::clean($request->introduction);
        $project->analysis = Purifier::clean($request->analysis);

        if ($request->user()->hasRole(['manager', 'admin', 'opponent'])) {
            User::where(['project_id' => $project->id])->update(['project_id' => null]);
            if ($request->users) {
                User::whereIn('id', $request->users)->update(['project_id' => $project->id]);
            }

            $project->state = $request->state;
        }
        $project->save();

        return redirect()->route('project.show', $id)->with([
            'title' => 'Úspěch',
            'body' => 'Projekt byl úspěšně upraven.',
            'type' => 'success'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     */
    public function destroy(Request $request)
    {
        if (!$request->ajax())
            abort(404, 'Not an ajax request');
        $id = $request->projekt;
        $files = ProjectFile::where('project_id', $id)->get();
        foreach ($files as $file) {
            $this->file->delete($file->original_name, $id);
        }
        User::where('project_id', $id)->update(['project_id' => null]);
        ProjectTask::where('project_id', $id)->delete();
        ProjectComment::where('project_id', $id)->delete();
        ReservedProject::where('project_id', $id)->delete();
        Project::find($id)->delete();
    }

    /**
     * @param int $type
     * @return \Illuminate\Http\JsonResponse
     */
    private function validateUpload($data, $type)
    {
        $validator = Validator::make($data, ProjectFile::getValidationRulesByType($type), [
            'file.max' => 'Soubor musí mít pod :max MB.',
            'file.required' => 'Soubor musí být zadán.',
            'file.file' => 'Soubor se nepodařilo zpracovat.',
            'file.image' => 'Soubor musí být obrázek',
            'type.required' => 'Špatně zadaný typ'
        ]);

        if ($validator->fails()) {
            return Response::json([
                'error' => true,
                'message' => $validator->messages()->first(),
                'code' => 400
            ], 400);
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @param int $type
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload(Request $request, $id, $type = 0)
    {
        $data = $request->all();
        $validationError = $this->validateUpload($data, $type);
        if (isset($validationError))
            return $validationError;

        if ($type == 2) {
            $project = Project::with('users')->find($id);
            Notification::send($project->users,
                new NewReportNotification(
                    ['title' => 'Nový posudek', 'body' => 'Na projektu přibyl nový posudek', 'type' => 'info',
                        'icon' => 'file', 'url' => route('project.show', $id)],
                    $project));
        }

        return $this->file->upload($data, $id, $type);
    }

    /**
     * @param Request $request
     * @param $id
     * @param int $type
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadProject(Request $request, $id, $type = 3)
    {
        $data = $request->all();
        $validationError = $this->validateUpload($data, $type);
        if (isset($validationError))
            return $validationError;
        $files = ProjectFile::where(['project_id' => $id, 'type' => $type])->orderBy('created_at', 'desc')->get();
        if (count($files) >= 2) {
            $this->file->delete($files[0]->filename, $id);
        }

        return $this->file->upload($data, $id, $type);
    }

    /**
     * @param $id
     * @param int $type
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUploads($id, $type = 0)
    {
        $files = ProjectFile::where(['project_id' => $id, 'type' => $type])->get();
        $url = URL::to('storage/project');

        $responseFiles = [];

        foreach ($files as $file) {
            $responseFiles[] = [
                'original' => $file->original_name,
                'server' => $file->filename,
                'size' => \Storage::disk('project')->size($file->filename),
                'url' => $url . '/' . $file->filename,
                'type' => $type
            ];
        }

        return response()->json([
            'files' => $responseFiles
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return int
     */
    public function destroyUpload(Request $request, $id)
    {
        $filename = $request->get('id');

        if (!$filename) {
            return 0;
        }

        return $this->file->delete($filename, $id);
    }

    /**
     * @param null $hash
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showMyProject($hash = null)
    {
        return redirect(route('project.show', Auth::user()->project_id) . $hash);
    }

    /**
     * @param $project
     * @return array
     */
    private function makeBreadcrumb($project)
    {
        return ['href' => route('project.show', $project->id), 'breadcrumb' => $project->name];
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function approve(Request $request, $id)
    {
        $project = Project::find($id);
        if (!$request->user()->hasRole(['opponent', 'manager', 'admin']) && $project->getOriginal('state') == 0) {
            abort(401, 'Nemáte dostatečné oprávnění.');
        }
        $project->update(['state' => 1]);

        Notification::send(User::where('project_id', $project->id)->get(),
            new StateChangeProjectNotification($project));

        return redirect()->route('project.show', $id)->with([
            'title' => 'Úspěch',
            'body' => 'Projekt byl schválen.',
            'type' => 'success'
        ]);
    }
}
