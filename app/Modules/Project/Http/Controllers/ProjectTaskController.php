<?php

namespace App\Modules\Project\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Project\Events\TaskChange;
use App\Modules\Project\Jobs\NotifyAboutNewTask;
use App\Modules\Project\Jobs\NotifyAboutTaskStateChange;
use App\Modules\Project\Models\Project;
use App\Modules\Project\Models\ProjectTask;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ProjectTaskController extends Controller
{

    /**
     * @param $projectId
     * @return mixed
     */
    public function get($projectId)
    {
        return ProjectTask::with('user')->orderBy('state', 'asc')->orderBy('created_at', 'asc')->where('project_id', $projectId)->get();
    }

    public function getTasks(Request $request)
    {
        if(!$request->user()->project_id) return;
        return view('project::tasks', ['route' => route('project.my', '#tasks'), 'tasks' =>
            ProjectTask::where([['project_id', '=', $request->user()->project->id], ['state', '!=', '3']])->get()]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function changeState(Request $request, $id)
    {
        $task = ProjectTask::where(['id' => $request->task_id, 'project_id' => $id]);
        $isUserSuper = $request->user()->hasRole(['opponent', 'admin', 'manager']);
        if ($task->first()->state == $request->state) return;

        if ($request->user()->id == $task->first()->created_by && $request->state == 2) {
            $update = ['state' => 3];
        } else if ($isUserSuper) {
            $update = ['state' => $request->state];
        } else if ($request->state <= 2) {
            $update = ['state' => $request->state];
        } else
            abort(300);

        if ($request->user()->id != $task->first()->created_by || $isUserSuper)
            dispatch(new NotifyAboutTaskStateChange($task->first(), $request->state, $id));

        $task->update($update);
        event(new TaskChange($id));
        return;
    }

    /**
     * @param Request $request
     * @return ProjectTask
     * I'm not really proud of this and next method of this controller
     * but as long as it works ill refactor it later on
     */
    public function store(Request $request, $id)
    {
        $this->validate($request, [
            'body' => 'required',
            'due' => 'sometimes|date_format:d.m.Y'
        ]);

        if ($request->task_id != null) {
            if ($request->user()->hasRole(['admin', 'manager', 'opponent'])) {
                ProjectTask::where(['id' => $request->task_id, 'project_id' => $id])
                    ->update(['body' => $request->body,
                        'due' => !empty($request->due) ? Carbon::createFromFormat('d.m.Y', $request->due)->format('Y-m-d') : null]);
                event(new TaskChange($id));
                return;
            }

            ProjectTask::where(['id' => $request->task_id, 'project_id' => $id, 'created_by' => $request->user()->id])
                ->update(['body' => $request->body, 'due' => !empty($request->due) ? Carbon::createFromFormat('d.m.Y', $request->due)->format('Y-m-d') : null]);
            event(new TaskChange($id));
            return;
        }

        $project = Project::find($id);
        if ($project->id != $request->user()->project_id)
            dispatch(new NotifyAboutNewTask($project));

        ProjectTask::create([
            'project_id' => $id,
            'body' => $request->body,
            'created_by' => $request->user()->id,
            'due' => $request->due,
            'state' => 1
        ]);
        event(new TaskChange($id));
        return $project;
    }

    /**
     * @param Request $request
     * @param $id
     */
    public function delete(Request $request, $id)
    {
        if ($request->user()->hasRole(['manager', 'opponent', 'admin'])) {
            ProjectTask::where(['project_id' => $id,
                'id' => $request->task_id])->delete() ? '' : abort(400);
            event(new TaskChange($id));
            return;
        }

        $task = ProjectTask::where(['project_id' => $id, 'id' => $request->task_id])->first();

        if ($task->state['name'] == 'Dokončen' || $request->user()->id == $task->created_by) {
            $task->delete() ? '' : abort(400);
            event(new TaskChange($id));
            return;
        } else {
            abort(400);
        }
    }
}