<?php

namespace App\Modules\Project\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectRequest extends FormRequest
{
    public function messages()
    {
        return [
            'name.required' => 'Název projektu musí být vyplněn.',
            'name_en.required' => 'Název projektu musí být vyplněn.',
            'name.max' => 'Název projektu nesmí být delší než 32 písmen.',
            'name_en.max' => 'Anglický název projektu nesmí být delší než 32 písmen.',
            'max_users.required' => 'Musí být zadán maximální počet žáků',
            'max_users.integer' => 'Maximální počet žáků musí být celé číslo',
            'max_users.max' => 'Maximální počet uživatelů na jednom projektu je 8.',
            'max_users.min' => 'Ty na tom projektu jako dělat nebudeš?'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:32',
            'name_en' => 'required|max:32',
            'max_users' => 'required|integer|max:8|min:1',
            'category_id' => 'required|exists:categories,id'
        ];
    }
}
