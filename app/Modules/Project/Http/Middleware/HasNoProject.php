<?php

namespace App\Modules\Project\Http\Middleware;

use Closure;

class HasNoProject
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->project_id != null) {
            abort(403, 'Uživatel již má přidělený projekt');
        }

        return $next($request);
    }
}
