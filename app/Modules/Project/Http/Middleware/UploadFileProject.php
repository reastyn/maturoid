<?php

namespace App\Modules\Project\Http\Middleware;

use Closure;

class UploadFileProject
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->type == 2) {
            if($request->user()->hasRole(['opponent', 'admin', 'manager'])) {
                return $next($request);
            } else {
                return abort(403, 'Na tuto operaci nemáte oprávnění.');
            }
        }

        return $next($request);
    }
}
