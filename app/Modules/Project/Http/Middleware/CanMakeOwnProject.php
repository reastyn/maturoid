<?php

namespace App\Modules\Project\Http\Middleware;

use App\Modules\Project\Models\Project;
use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class CanMakeOwnProject
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        if ($user->hasRole(['opponent', 'manager', 'admin']) ||
            ($user->hasRole('student') && $user->project_id == null &&
                ($user->opponent_id != null || $user->manager_id != null))
        ) {
            return $next($request);
        }

        return abort(403, 'Na tuto operaci nemáte oprávnění.');
    }
}