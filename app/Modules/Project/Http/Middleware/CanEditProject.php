<?php

namespace App\Modules\Project\Http\Middleware;

use App\Modules\Project\Models\Project;
use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class CanEditProject
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        if ($user->project_id == $request->projekt) {
            $request->attributes->add(['canEdit' => true]);
            return $next($request);
        }

        if($user->hasRole('admin')) {
            $request->attributes->add(['canEdit' => true]);
            return $next($request);
        }

        if ($user->hasRole('opponent') ||
            $user->hasRole('manager')
        ) {
            $id = $request->user()->id;
            $projectIds = User::where('opponent_id', $id)->orWhere('manager_id', $id)->pluck('project_id')->toArray();
            $ownedProjectIds = Project::where('created_by', $id)->pluck('id')->toArray();
            if(in_array($request->projekt, array_merge($projectIds, $ownedProjectIds))) {
                $request->attributes->add(['canEdit' => true]);
                return $next($request);
            }
        }

        return abort(403, 'Na tuto operaci nemáte oprávnění.');
    }
}
