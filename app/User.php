<?php

namespace App;

use App\Helpers\ProfileImage;
use App\Modules\Auth\Jobs\CreateUserImage;
use App\Modules\Auth\Notifications\NewUserNotification;
use App\Modules\Project\Models\Project;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use URL;

class User extends Authenticatable
{
    use Notifiable, HasRoles;

    private $storage;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->storage = URL::to('storage/profile');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'profile', 'opponent_id', 'manager_id', 'project_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = ['avatar_url'];

    /**
     * Get the administrator flag for the user.
     *
     * @return bool
     */
    public function getAvatarUrlAttribute()
    {
        return $this->getAvatar();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function opponent()
    {
        return $this->hasOne(User::class, 'id', 'opponent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function manager()
    {
        return $this->hasOne(User::class, 'id', 'manager_id');
    }

    public function project()
    {
        return $this->hasOne(Project::class, 'id', 'project_id');
    }

    /**
     * @return bool
     */
    public function hasPassword()
    {
        if ($this->getAttribute('password')) {
            return true;
        }
        return false;
    }

    /**
     * @param $role
     * @return mixed
     */
    public static function getAllWithRole($role)
    {
        return self::whereHas('roles', function ($q) use ($role) {
            $q->where('role', $role);
        })->get();
    }

    /**
     * @param $name
     * @param $email
     * @param null $password
     * @return static
     */
    public static function createUser($name, $email, $password = null)
    {
        $user = self::create([
            'email' => $email,
            'name' => $name,
            'password' => $password ? bcrypt($password) : '',
        ]);
        dispatch(new CreateUserImage(new ProfileImage, $user));
        $user->assignRole('visitor');
        $user->notify(new NewUserNotification(['title' => 'Váš účet byl úspěšně založen',
            'body' => 'Od teď můžete používat aplikaci maturoid', 'type' => 'success', 'url' => route('home')]));
        return $user;
    }

    /**
     * Determine if the user has (one of) the given role(s).
     *
     * @param string|array|Role|\Illuminate\Support\Collection $roles
     *
     * @return bool
     */
    public function hasRole($roles)
    {
        if (is_string($roles)) {
            return $this->roles->contains('role', $roles);
        }

        if ($roles instanceof Role) {
            return $this->roles->contains('id', $roles->id);
        }

        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }

            return false;
        }

        return (bool)$roles->intersect($this->roles)->count();
    }

    /**
     * @param $value
     */
    public function setOpponentIdAttribute($value)
    {
        $this->attributes['opponent_id'] = $value ?: null;
    }

    /**
     * @param $value
     */
    public function setManagerIdAttribute($value)
    {
        $this->attributes['manager_id'] = $value ?: null;
    }

    /**
     * @param int $size
     * @return string
     */
    public function getAvatar($size = 100)
    {
        return $this->storage . '/' . $this->profile . '/' . $size . '.jpg';
    }
}
