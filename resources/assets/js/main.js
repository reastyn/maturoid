import Vue from 'vue/dist/vue.js';
import FormError from './components/FormError.vue';

new Vue({
    el: '#maturoid',

    components: {
        FormError
    },

    data: {
        errors: {},
    },

    methods: {
        validateForm(e) {
            let el = e.srcElement;
            let method = el.querySelector('input[name="_method"]');
            let requestType = ( method ? method.value : el.method).toLowerCase();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('#token').attr('content')
                }
            });

            $.ajax({
                method: requestType,
                url: el.action,
                data: $(el).serialize()
            })
                .done((res) => {
                    console.log(res);
                })
                .fail((err) => {
                    console.log(err.responseJSON);
                    this.errors = err.responseJSON;
                    // this.$set('errors', err.responseJSON);
                });
        }
    }
});