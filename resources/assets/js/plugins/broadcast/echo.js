var classCallCheck = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

var createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

var inherits = function (subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
};

var possibleConstructorReturn = function (self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (typeof call === "object" || typeof call === "function") ? call : self;
};

var Connector = function () {
    function Connector(options) {
        classCallCheck(this, Connector);

        this._defaultOptions = {
            auth: {
                headers: {}
            },
            authEndpoint: '/broadcasting/auth',
            connector: 'pusher',
            csrfToken: null,
            host: null,
            key: null,
            namespace: null
        };
        this.setOptions(options);
        this.connect();
    }

    createClass(Connector, [{
        key: 'setOptions',
        value: function setOptions(options) {
            this.options = _extends(this._defaultOptions, options);
            if (this.csrfToken()) {
                this.options.auth.headers['X-CSRF-TOKEN'] = this.csrfToken();
            }
            return options;
        }
    }, {
        key: 'csrfToken',
        value: function csrfToken() {
            var selector = document.querySelector('meta[name="csrf-token"]');
            if (window['Laravel'] && window['Laravel'].csrfToken) {
                return window['Laravel'].csrfToken;
            } else if (this.options.csrfToken) {
                return this.options.csrfToken;
            } else if (selector) {
                return selector.getAttribute('content');
            }
            return null;
        }
    }]);
    return Connector;
}();

var Channel = function () {
    function Channel() {
        classCallCheck(this, Channel);
    }

    createClass(Channel, [{
        key: 'notification',
        value: function notification(callback) {
            return this.listen('.Illuminate.Notifications.Events.BroadcastNotificationCreated', callback);
        }
    }]);
    return Channel;
}();

var EventFormatter = function () {
    function EventFormatter() {
        classCallCheck(this, EventFormatter);

        this.defaultNamespace = 'App.Events';
    }

    createClass(EventFormatter, [{
        key: 'format',
        value: function format(event) {
            if (event.charAt(0) != '\\' && event.charAt(0) != '.') {
                event = this.defaultNamespace + '.' + event;
            } else {
                event = event.substr(1);
            }
            return event.replace(/\./g, '\\');
        }
    }, {
        key: 'namespace',
        value: function namespace(value) {
            this.defaultNamespace = value;
        }
    }]);
    return EventFormatter;
}();

var PusherChannel = function (_Channel) {
    inherits(PusherChannel, _Channel);

    function PusherChannel(channel, options) {
        classCallCheck(this, PusherChannel);

        var _this = possibleConstructorReturn(this, Object.getPrototypeOf(PusherChannel).call(this));

        _this.channel = channel;
        _this.options = options;
        _this.eventFormatter = new EventFormatter();
        if (_this.options.namespace) {
            _this.eventFormatter.namespace(_this.options.namespace);
        }
        return _this;
    }

    createClass(PusherChannel, [{
        key: 'listen',
        value: function listen(event, callback) {
            this.bind(this.eventFormatter.format(event), callback);
            return this;
        }
    }, {
        key: 'bind',
        value: function bind(event, callback) {
            this.channel.bind(event, callback);
        }
    }]);
    return PusherChannel;
}(Channel);

var PusherPresenceChannel = function (_PusherChannel) {
    inherits(PusherPresenceChannel, _PusherChannel);

    function PusherPresenceChannel() {
        classCallCheck(this, PusherPresenceChannel);
        return possibleConstructorReturn(this, Object.getPrototypeOf(PusherPresenceChannel).apply(this, arguments));
    }

    createClass(PusherPresenceChannel, [{
        key: 'here',
        value: function here(callback) {
            var _this2 = this;

            this.bind('pusher:subscription_succeeded', function (data) {
                callback(Object.keys(data.members).map(function (k) {
                    return data.members[k];
                }), _this2.channel);
            });
            return this;
        }
    }, {
        key: 'joining',
        value: function joining(callback) {
            var _this3 = this;

            this.bind('pusher:member_added', function (member) {
                callback(member.info, _this3.channel);
            });
            return this;
        }
    }, {
        key: 'leaving',
        value: function leaving(callback) {
            var _this4 = this;

            this.bind('pusher:member_removed', function (member) {
                callback(member.info, _this4.channel);
            });
            return this;
        }
    }]);
    return PusherPresenceChannel;
}(PusherChannel);

var SocketIoChannel = function (_Channel) {
    inherits(SocketIoChannel, _Channel);

    function SocketIoChannel(channel, options) {
        classCallCheck(this, SocketIoChannel);

        var _this = possibleConstructorReturn(this, Object.getPrototypeOf(SocketIoChannel).call(this));

        _this.channel = channel;
        _this.options = options;
        _this.eventFormatter = new EventFormatter();
        if (_this.options.namespace) {
            _this.eventFormatter.namespace(_this.options.namespace);
        }
        return _this;
    }

    createClass(SocketIoChannel, [{
        key: 'listen',
        value: function listen(event, callback) {
            this.on(this.eventFormatter.format(event), callback);
            return this;
        }
    }, {
        key: 'on',
        value: function on(event, callback) {
            this.channel.on(event, callback);
        }
    }]);
    return SocketIoChannel;
}(Channel);

var SocketIoPresenceChannel = function (_SocketIoChannel) {
    inherits(SocketIoPresenceChannel, _SocketIoChannel);

    function SocketIoPresenceChannel() {
        classCallCheck(this, SocketIoPresenceChannel);
        return possibleConstructorReturn(this, Object.getPrototypeOf(SocketIoPresenceChannel).apply(this, arguments));
    }

    createClass(SocketIoPresenceChannel, [{
        key: 'here',
        value: function here(callback) {
            var _this2 = this;

            this.on('presence:subscribed', function (members) {
                callback(members.map(function (m) {
                    return m.user_info;
                }), _this2.channel);
            });
            return this;
        }
    }, {
        key: 'joining',
        value: function joining(callback) {
            var _this3 = this;

            this.on('presence:joining', function (member) {
                callback(member.user_info, _this3.channel);
            });
            return this;
        }
    }, {
        key: 'leaving',
        value: function leaving(callback) {
            var _this4 = this;

            this.on('presence:leaving', function (member) {
                callback(member.user_info, _this4.channel);
            });
            return this;
        }
    }]);
    return SocketIoPresenceChannel;
}(SocketIoChannel);

var PusherConnector = function (_Connector) {
    inherits(PusherConnector, _Connector);

    function PusherConnector() {
        var _Object$getPrototypeO;

        classCallCheck(this, PusherConnector);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        var _this = possibleConstructorReturn(this, (_Object$getPrototypeO = Object.getPrototypeOf(PusherConnector)).call.apply(_Object$getPrototypeO, [this].concat(args)));

        _this.channels = [];
        return _this;
    }

    createClass(PusherConnector, [{
        key: 'connect',
        value: function connect() {
            this.pusher = new Pusher(this.options.key, this.options);
        }
    }, {
        key: 'listen',
        value: function listen(channel, event, callback) {
            return this.channel(channel).listen(event, callback);
        }
    }, {
        key: 'channel',
        value: function channel(_channel) {
            return new PusherChannel(this.createChannel(_channel), this.options);
        }
    }, {
        key: 'privateChannel',
        value: function privateChannel(channel) {
            return new PusherChannel(this.createChannel('private-' + channel), this.options);
        }
    }, {
        key: 'presenceChannel',
        value: function presenceChannel(channel) {
            return new PusherPresenceChannel(this.createChannel('presence-' + channel), this.options);
        }
    }, {
        key: 'createChannel',
        value: function createChannel(channel) {
            if (!this.channels[channel]) {
                this.channels[channel] = this.subscribe(channel);
            }
            return this.channels[channel];
        }
    }, {
        key: 'leave',
        value: function leave(channel) {
            var _this2 = this;

            var channels = [channel, 'private-' + channel, 'presence-' + channel];
            channels.forEach(function (channelName, index) {
                if (_this2.channels[channelName]) {
                    _this2.unsubscribe(channelName);
                    delete _this2.channels[channelName];
                }
            });
        }
    }, {
        key: 'subscribe',
        value: function subscribe(channel) {
            return this.pusher.subscribe(channel);
        }
    }, {
        key: 'unsubscribe',
        value: function unsubscribe(channel) {
            this.pusher.unsubscribe(channel);
        }
    }, {
        key: 'socketId',
        value: function socketId() {
            return this.pusher.connection.socket_id;
        }
    }]);
    return PusherConnector;
}(Connector);

var SocketIoConnector = function (_Connector) {
    inherits(SocketIoConnector, _Connector);

    function SocketIoConnector() {
        var _Object$getPrototypeO;

        classCallCheck(this, SocketIoConnector);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        var _this = possibleConstructorReturn(this, (_Object$getPrototypeO = Object.getPrototypeOf(SocketIoConnector)).call.apply(_Object$getPrototypeO, [this].concat(args)));

        _this.channels = [];
        return _this;
    }

    createClass(SocketIoConnector, [{
        key: 'connect',
        value: function connect() {
            this.socket = io(this.options.host);
            return this.socket;
        }
    }, {
        key: 'listen',
        value: function listen(channel, event, callback) {
            return this.channel(channel).listen(event, callback);
        }
    }, {
        key: 'channel',
        value: function channel(_channel) {
            return new SocketIoChannel(this.createChannel(_channel), this.options);
        }
    }, {
        key: 'privateChannel',
        value: function privateChannel(channel) {
            return new SocketIoChannel(this.createChannel('private-' + channel), this.options);
        }
    }, {
        key: 'presenceChannel',
        value: function presenceChannel(channel) {
            return new SocketIoPresenceChannel(this.createChannel('presence-' + channel), this.options);
        }
    }, {
        key: 'createChannel',
        value: function createChannel(channel) {
            if (!this.channels[channel]) {
                this.channels[channel] = this.subscribe(channel);
            }
            return this.channels[channel];
        }
    }, {
        key: 'leave',
        value: function leave(channel) {
            var _this2 = this;

            var channels = [channel, 'private-' + channel, 'presence-' + channel];
            channels.forEach(function (channelName, index) {
                if (_this2.channels[channelName]) {
                    _this2.unsubscribe(channelName);
                    delete _this2.channels[channelName];
                }
            });
        }
    }, {
        key: 'subscribe',
        value: function subscribe(channel) {
            return this.socket.emit('subscribe', {
                channel: channel,
                auth: this.options.auth || {}
            });
        }
    }, {
        key: 'unsubscribe',
        value: function unsubscribe(channel) {
            this.socket.removeAllListeners();
            this.socket.emit('unsubscribe', {
                channel: channel,
                auth: this.options.auth || {}
            });
        }
    }, {
        key: 'socketId',
        value: function socketId() {
            return this.socket.id;
        }
    }]);
    return SocketIoConnector;
}(Connector);

var Echo = function () {
    function Echo(options) {
        classCallCheck(this, Echo);

        this.options = options;
        if (typeof Vue === 'function' && Vue.http) {
            this.registerVueRequestInterceptor();
        }
        if (this.options.broadcaster == 'pusher') {
            if (!window['Pusher']) {
                window['Pusher'] = require('pusher-js');
            }
            this.connector = new PusherConnector(this.options);
        } else if (this.options.broadcaster == 'socket.io') {
            this.connector = new SocketIoConnector(this.options);
        }
    }

    createClass(Echo, [{
        key: 'registerVueRequestInterceptor',
        value: function registerVueRequestInterceptor() {
            var _this = this;

            Vue.http.interceptors.push(function (request, next) {
                if (_this.socketId()) {
                    request.headers['X-Socket-ID'] = _this.socketId();
                }
                next();
            });
        }
    }, {
        key: 'listen',
        value: function listen(channel, event, callback) {
            return this.connector.listen(channel, event, callback);
        }
    }, {
        key: 'channel',
        value: function channel(_channel) {
            return this.connector.channel(_channel);
        }
    }, {
        key: 'private',
        value: function _private(channel) {
            return this.connector.privateChannel(channel);
        }
    }, {
        key: 'join',
        value: function join(channel) {
            return this.connector.presenceChannel(channel);
        }
    }, {
        key: 'leave',
        value: function leave(channel) {
            this.connector.leave(channel);
        }
    }, {
        key: 'socketId',
        value: function socketId() {
            return this.connector.socketId();
        }
    }]);
    return Echo;
}();

module.exports = Echo;