export default function init() {
    $.ajaxSetup({
        headers:
        { 'X-CSRF-TOKEN': $('#token').attr('content') }
    });

    maturoid.handleNotifications();
    maturoid.handleTasks();
}