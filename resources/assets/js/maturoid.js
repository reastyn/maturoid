import Echo from "laravel-echo"
import scripts from "./init.js"
var maturoid = window.maturoid || {};

var stack = {
    "dir1": "up", "dir2": "left",
    "spacing1": 0, "spacing2": 0,
    "firstpos1": 15, "firstpos2": 15
};

export default class Maturoid {
    constructor() {
        this.echo = new Echo({
            broadcaster: 'pusher',
            key: '68d6547deb2ae2667392',
            cluster: 'eu'
        });

        this.echo.private('App.User.' + $('#user-id').attr('content'))
            .notification(({title, body, style}) => {
                this.notify(title, body, style);
                let label = $('#notificationMenu').find('.label');
                let text = label.text();
                label.text(parseInt(text != '' ? text : 0) + 1);
            });
    }

    notify(title, text, type, classes = '', delay = 7777) {
        return new PNotify({
            title: title,
            text: text,
            styling: 'bootstrap3',
            type,
            addclass: 'stack-bottomright',
            delay: delay,
            stack
        })
    }

    handleNotifications() {
        let body = $('#notifications');
        $('#notificationMenu').on('show.bs.dropdown', () => {
            this.getNotifications(body);
        });

        $('.markAllRead').on('click', event => {
            event.stopPropagation();
            $.post( '/notifications/markRead', () => {
                this.getNotifications(body);
                $('#notificationMenu').find('.label').text('');
                this.notify('Hotovo', 'Všechny notifikace byly přečteny.', 'success');
            });
        });
    }

    handleTasks() {
        let body = $('#tasksNavbar');
        $('#taskMenu').on('show.bs.dropdown', () => {
            $.get('/tasks', data => {
                body.html(data);
            });
        });
    }

    getNotifications(body) {
        $.get('/notifications', data => {
            body.html(data);
            this.onNotificationClick();
        });
    }

    onNotificationClick() {
        $('.notification a').on('click', function(event) {
            event.preventDefault();
            $.post( '/notifications', {id: $(this).data('id')}, () => {
                location.href = $(this).attr('href');
            });
        });
    }
}

window.maturoid = new Maturoid;

$(document).ready(function () {
    scripts();
});