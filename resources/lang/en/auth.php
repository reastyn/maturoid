<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Zadané údaje se neschodují.',
    'throttle' => 'Až moc pokusů o přihlášení zkuste to prosím za :seconds sekund.',

];
