<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Hesla musí mít nejméně 6 znaků.',
    'reset' => 'Vaše heslo bylo úspěšně resetováno!',
    'sent' => 'Odeslali jsme vám email s změněním hesla!',
    'token' => 'Tento token není validní.',
    'user' => "Nemůžeme najít uživatele s touto emailovou adresou.",

];
