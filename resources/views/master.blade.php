<!DOCTYPE html>
<html lang="en">
<head>
    @php ($breadcrumbs = collect(Request::route()->getAction()['breadcrumb']))
    @php ($notificationCount = Auth::user()->notifications->where('read_at', null)->count())
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta id="token" name="csrf-token" content="{{ csrf_token() }}">
    <meta id="user-id" content="{{ Auth::getUser()->id }}">
    <title>Maturoid > {{ $breadcrumbs[0] }}</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    {{--<link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">--}}
    <link href="{{ asset('css/maturoid.css') }}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    {{--<!-- Theme JS files -->
    <script type="text/javascript" src="assets/js/plugins/visualization/d3/d3.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
    --}}{{--<script type="text/javascript" src="assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>--}}{{--
    <script type="text/javascript" src="assets/js/plugins/pickers/daterangepicker.js"></script>

    <script type="text/javascript" src="assets/js/pages/dashboard.js"></script>--}}
<!-- /theme JS files -->
    @yield('head')
</head>
<body class="skin-blue sidebar-mini">
<div class="wrapper" id="maturoid">
    @include('navbar')

    @include('sidebar')
    <div class="content-wrapper">
        @include('header')
        <section class="content">
            @yield('content')
        </section>
    </div>

    @include('footer')
</div>

<!-- Core JS files -->
<script type="text/javascript" src="{{ asset('js/all.js') }}"></script>
<!-- /core JS files -->
@stack('scripts')
@if(Session::has('title'))
    <script>
        maturoid.notify('{{ Session::get('title') }}', '{{ Session::get('body') }}', '{{ Session::get('type') }}');
    </script>
@endif
</body>
</html>