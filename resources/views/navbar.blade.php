<header class="main-header">
    <!-- Logo -->
    <a href="{{ route('home') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><i class="fa fa-cloud-upload" aria-hidden="true"></i></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><i class="fa fa-cloud-upload" aria-hidden="true"></i> Maturoid</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                <li class="dropdown messages-menu" id="notificationMenu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-envelope-o"></i>
                        <span class="label label-success">@if($notificationCount > 0) {{ $notificationCount }} @endif</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">
                            Notifikace
                            <a href="#" class="markAllRead">Označit vše jako přečtené</a>
                        </li>
                        <li id="notifications">
                            <div class="text-center">
                                <i class="fa fa-refresh fa-spin"></i>
                            </div>
                        </li>
                        <li class="footer"><a href="{{ route('notifications.index') }}">Zobrazit všechny notifikace</a></li>
                    </ul>
                </li>
                <!-- Notifications: style can be found in dropdown.less -->
                @if(Auth::user()->project_id)
                <li class="dropdown notifications-menu" id="taskMenu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-tasks"></i>
                        <span class="label label-warning"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li id="tasksNavbar">
                            <div class="text-center">
                                <i class="fa fa-refresh fa-spin"></i>
                            </div>
                        </li>
                        <li class="footer"><a href="{{ route('project.my', '#tasks') }}">Zobrazit všechny</a></li>
                    </ul>
                </li>
                @endif
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img class="user-image" alt="User Image"
                             src="{{ Auth::user()->getAvatar() }}"
                             onerror="if (this.src != '100.jpg') this.src = '{{ URL::to('storage/profile') }}/100.jpg';">
                        <span class="hidden-xs">{{ Auth::getUser()->name }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img class="img-circle" alt="User Image"
                                 src="{{ Auth::user()->getAvatar(500) }}"
                                 onerror="if (this.src != '100.jpg') this.src = '{{ URL::to('storage/profile') }}/100.jpg';">

                            <p>
                                {{ Auth::getUser()->name }}
                                <small>{{ Auth::getUser()->roles->implode('name', ', ') }}</small>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-right">
                                <a href="{{ route('logout') }}" class="btn btn-default btn-flat">Odhlásit se</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
            </ul>
        </div>
    </nav>
</header>