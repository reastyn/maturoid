@extends('master')

@section('content')
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua">
                    <i class="fa fa-bell"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Nové notifikace</span>
                    <span class="info-box-number">{{ Auth::user()->notifications->where('read_at', null)->count() }}</span>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green">
                    <i class="fa fa-folder-open-o"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Volné projekty</span>
                    <span class="info-box-number">{{ $reservedProjects }}</span>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red">
                    <i class="fa fa-folder-o"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Projektů</span>
                    <span class="info-box-number">{{ $projectCount }}</span>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-yellow">
                    <i class="fa fa-user"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Uživatelů</span>
                    <span class="info-box-number">{{ $usersCount }}</span>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Naposledy registrovaní uživatelé</h3>
                </div>
                <div class="box-body no-padding">
                    <ul class="users-list clearfix">
                        @foreach($users as $user)
                            <li>
                                <img src="{{ $user->getAvatar(500) }}" alt="{{ $user->name }}">
                                <a href="#" class="users-list-name">{{ $user->name }}</a>
                                <span class="users-list-date">{{ $user->created_at->diffForHumans() }}</span>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="info-box bg-blue">
                <span class="info-box-icon">
                    <i class="fa fa-file"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Soubory</span>
                    <span class="info-box-number">{{ $fileCount }} souborů</span>
                    <span class="progress-description">uloženo na systému maturoid</span>
                </div>
            </div>
            <div class="info-box bg-red">
                <span class="info-box-icon">
                    <i class="fa fa-check"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Úkoly</span>
                    <span class="info-box-number">{{ $taskCount }} úkolů</span>
                    <span class="progress-description">je právě řešeno přes maturoid</span>
                </div>
            </div>
            <div class="info-box bg-green">
                <span class="info-box-icon">
                    <i class="fa fa-comment"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Komentáře</span>
                    <span class="info-box-number">{{ $commentCount }} komentářů</span>
                    <span class="progress-description">bylo přidáno při komunikaci na projektu</span>
                </div>
            </div>
            <div class="info-box bg-yellow">
                <span class="info-box-icon">
                    <i class="fa fa-bell"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Notifikace</span>
                    <span class="info-box-number">{{ $notificationCount }} notifikací</span>
                    <span class="progress-description">přišlo uživatelům v rámci práce na projektu</span>
                </div>
            </div>
        </div>
    </div>
@endsection