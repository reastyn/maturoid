@extends('master')

@section('content')
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua">
                    <i class="fa fa-bell"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Nové notifikace</span>
                    <span class="info-box-number">{{ Auth::user()->notifications->where('read_at', null)->count() }}</span>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green">
                    <i class="fa fa-handshake-o"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Rezervace</span>
                    <span class="info-box-number">{{ $reserveProjectsCount }}</span>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red">
                    <i class="fa fa-address-book"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Přidělené projekty</span>
                    <span class="info-box-number">{{ $projectCount }}</span>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-yellow">
                    <i class="fa fa-user"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-number">{{ Auth::user()->name }}</span>
                    <span class="info-box-text">{{ Auth::user()->roles->implode('name', ', ') }}</span>
                    <button class="btn btn-primary btn-block"
                            data-toggle="modal" data-target="#profile_modal"><b>Změnit obrázek</b>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Přidělené projekty</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <ul class="products-list product-list-in-box">
                        @foreach($projects as $project)
                            <li class="item">
                                <div class="product-img">
                                    <img src="{{ $project->users->first()->avatar_url }}" alt="">
                                </div>
                                <div class="project-info">
                                    <a href="{{ route('project.show', $project->id) }}"
                                       class="product-title">{{ $project->name }}
                                        <span class="label label-info pull-right">{{ $project->category->name }}</span></a>
                                    <span class="product-description">
                              Studenti: {{ $project->users->implode('name', ', ') }}
                        </span>
                                </div>
                            </li>
                    @endforeach
                    <!-- /.item -->
                    </ul>
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                    <a href="{{ route('manageProject.index') }}" class="uppercase">Všechny projekty</a>
                </div>
                <!-- /.box-footer -->
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Správa rezervace projektů</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    @if(!$reservedProjects->isEmpty())
                    <div class="table-responsive">
                        <table class="table no-margin">
                            <thead>
                            <tr>
                                <th>Projekt</th>
                                <th>Uživatel</th>
                                <th>Akce</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($reservedProjects as $reserved)
                            <tr>
                                <td><a href="{{ route('project.show', $reserved->project->id) }}">{{ $reserved->project->name }}</a></td>
                                <td>{{ $reserved->user->name }}</td>
                                <td>
                                    <a href="{{ route('reserveProject.approve', $reserved->id) }}" class="btn btn-xs btn-success">
                                        <i class="glyphicon glyphicon-edit"></i> Schválit</a>
                                    <a href="{{ route('reserveProject.deny', $reserved->id) }}" href="#" class="btn btn-xs btn-danger">
                                        <i class="glyphicon glyphicon-trash"></i> Zamítnout</a>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    @else
                        <div class="alert alert-info">
                            Všechny rezervace vyřízeny.
                        </div>
                    @endif
                    <!-- /.table-responsive -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <a href="{{ route('makeProject.index') }}" class="btn btn-sm btn-info btn-flat pull-left">Nový projekt</a>
                    <a href="{{ route('reserveProject.index') }}" class="btn btn-sm btn-default btn-flat pull-right">Zobrazit všechny rezervace</a>
                </div>
                <!-- /.box-footer -->
            </div>
        </div>
    </div>

    <div id="profile_modal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">Změnit profilovou fotografii</h5>
                </div>

                <div class="modal-body">
                    <p>Fotografie by měla mít méně než 1 MB.</p>
                    <form id="profile-picture" action="{{ route('profile.updateProfile') }}"
                          class="dropzone"></form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Zavřít</button>
                    <button type="button" class="btn btn-primary" id="save-profile-picture">Uložit</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script type="text/javascript" src="{{ asset('js/upload.js') }}"></script>
<script type="text/javascript">
    Dropzone.autoDiscover = false;
    $(document).ready(function () {
        var profile = new Dropzone("#profile-picture", {
            maxFilesize: 1,
            maxFiles: 1,
            dictDefaultMessage: 'Sem přesuňte profilový obrázek',
            dictFileTooBig: 'Obrázek je moc velký, maximální velikost je 1 MB',
            init: function () {
                this.on('addedfile', function (file) {
                    if (this.fileTracker) {
                        this.removeFile(this.fileTracker);
                    }
                    this.fileTracker = file;
                });
                this.on('success', function (file) {
                    $(file.previewElement).find('.dz-progress').hide();
                    maturoid.notify('Hotovo', 'Fotografie byla úspěšně změněna. Uvidíte ji po znovunačtení stránky.', 'success')
                });
                this.on('error', function (file) {
                    var el = $(file.previewElement);
                    el.find('.dz-progress').hide();
                    el.hover(function () {
                        el.find('.dz-error-mark').hide();
                    });
                });
            },
            autoProcessQueue: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#save-profile-picture').on('click', function () {
            profile.processQueue();
        });
    });
</script>
@endpush