@extends('master')

@section('content')
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua">
                    <i class="fa fa-bell"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Nové notifikace</span>
                    <span class="info-box-number">{{ Auth::user()->notifications->where('read_at', null)->count() }}</span>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green">
                    <i class="fa fa-handshake-o"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Rezervace</span>
                    <span class="info-box-number">{{ $reservedProjects }}</span>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red">
                    <i class="fa fa-folder-open-o"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Projektů</span>
                    <span class="info-box-number">{{ $projectCount }}</span>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-yellow">
                    <i class="fa fa-user"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Uživatelů</span>
                    <span class="info-box-number">{{ $usersCount }}</span>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Naposledy registrovaní uživatelé</h3>
                </div>
                <div class="box-body no-padding">
                    <ul class="users-list clearfix">
                        @foreach($users as $user)
                            <li>
                                <img src="{{ $user->getAvatar(500) }}" alt="{{ $user->name }}">
                                <a href="{{ route('users.edit', $user->id) }}"
                                   class="users-list-name">{{ $user->name }}</a>
                                <span class="users-list-date">{{ $user->created_at->diffForHumans() }}</span>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="box-footer text-center">
                    <a href="{{ route('users.index') }}" class="uppercase">Všichni uživatelé</a>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Naposledy aktualizované projekty</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <ul class="products-list product-list-in-box">
                        @foreach($projects as $project)
                            <li class="item">
                                @if($project->users->first())
                                    <div class="product-img">
                                        <img src="{{ $project->users->first()->avatar_url }}" alt="">
                                    </div>
                                    <div class="project-info">
                                @else
                                    <div class="product-img text-aqua">
                                        <i class="fa fa-folder"></i>
                                    </div>
                                    <div class="product-info">
                                @endif
                                    <a href="{{ route('project.show', $project->id) }}"
                                       class="product-title">{{ $project->name }}
                                        <span class="label label-info pull-right">{{ $project->category->name }}</span></a>
                                    <span class="product-description">
                              @if(!$project->users->isEmpty())
                                Studenti: {{ $project->users->implode('name', ', ') }}
                              @else
                                  Žádní přiřazení studenti
                              @endif
                        </span>
                                </div>
                            </li>
                    @endforeach
                    <!-- /.item -->
                    </ul>
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                    <a href="{{ route('manageProject.index') }}" class="uppercase">Všechny projekty</a>
                </div>
                <!-- /.box-footer -->
            </div>
        </div>
    </div>
@endsection