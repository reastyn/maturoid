@extends('master')

@section('content')
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua">
                    <i class="fa fa-bell"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Nové notifikace</span>
                    <span class="info-box-number">{{ Auth::user()->notifications->where('read_at', null)->count() }}</span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-yellow">
                    <i class="fa fa-tasks"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Úkoly</span>
                    <span class="info-box-number">{{ $taskCount }}</span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red">
                    <i class="fa fa-comment"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Komentáře za 24h</span>
                    <span class="info-box-number">{{ $commentCount }}</span>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green">
                    <i class="fa fa-check"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Dokončené úkoly</span>
                    <span class="info-box-number">{{ $completedTaskCount }}</span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Úkoly</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <ul class="products-list product-list-in-box">
                        @forelse($tasks as $task)
                            <li class="item">
                                <div class="product-img {{ $task->state['class'] }}">
                                    <i class="fa {{ $task->state['icon'] }}"></i>
                                </div>
                                <div class="product-info">
                                    <a href="{{ route('project.my', '#tasks') }}" class="product-title">{{ $task->body }}
                                        <span class="label label-info pull-right">{{ $task->due }}</span></a>
                                    <span class="product-description">
                              {{ $task->state['name'] }}
                        </span>
                                </div>
                            </li>
                            @empty
                            <div class="alert alert-success">
                                Žádné rozpracované úkoly
                            </div>
                    @endforelse
                    <!-- /.item -->
                    </ul>
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                    <a href="{{ route('project.my', '#tasks') }}" class="uppercase">Zobrazit úkoly</a>
                </div>
                <!-- /.box-footer -->
            </div>
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Posudky</h3>
                </div>
                <div class="box-body">
                    @if(!$reportFiles->isEmpty())
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <th>Soubor</th>
                                <th>Velikost</th>
                                <th style="width: 200px;">Akce</th>
                            </tr>
                            @foreach($reportFiles as $file)
                                <tr>
                                    <td>{{ $file->original_name }}</td>
                                    <td>{{ \App\Helpers\Utils::bytesToHuman($file->size) }}</td>
                                    <td>
                                        <a href="{{ URL::to('storage/project') . '/' . $file->filename }}"
                                           class="btn btn-sm btn-default">Prohlédnout</a>
                                        <a href="{{ URL::to('storage/project') . '/' . $file->filename }}"
                                           class="btn btn-sm btn-primary" download>Stáhnout</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="alert alert-danger">
                            Oponent / vedoucí ještě nepřidali žádné posudky.
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Můj projekt</h3>
                </div>
                <div class="box-body">
                    <dt>Název projektu</dt>
                    <dd>{{ $project->name }}</dd>
                    <dt>Název projektu (anglicky)</dt>
                    <dd>{{ $project->name_en }}</dd>
                    <dt>Přidělení na projektu</dt>
                    <dd>{{ $project->users->implode('name', ', ') }}</dd>
                    <dt>Maximální počet žáků</dt>
                    <dd>{{ $project->max_users }}</dd>
                    <dt>Datum vytvoření</dt>
                    <dd>{{ $project->created_at->format('Y-m-d H:i') }}</dd>
                    <dt>Datum poslední úpravy</dt>
                    <dd>{{ $project->updated_at->format('Y-m-d H:i') }}</dd>
                    <dt>Zaměření</dt>
                    <dd>{{ $project->category->name }}</dd>
                    <dt>Stav</dt>
                    <dd>{{ $project->state }}</dd>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <img class="profile-user-img img-responsive img-circle"
                         src="{{ Auth::user()->getAvatar(500) }}"
                         alt="User profile picture">

                    <h3 class="profile-username text-center">{{ Auth::user()->name }}</h3>

                    <p class="text-muted text-center">{{ Auth::user()->roles->implode('name', ', ')  }}</p>

                    <ul class="list-group list-group-unbordered">
                        @if(Auth::user()->opponent)
                            <li class="list-group-item">
                                <b>Oponent</b> <a class="pull-right">{{ Auth::user()->opponent->name }}</a>
                            </li>
                        @endif
                        @if(Auth::user()->manager)
                            <li class="list-group-item">
                                <b>Vedoucí</b> <a class="pull-right">{{ Auth::user()->manager->name }}</a>
                            </li>
                        @endif
                    </ul>

                    <button class="btn btn-primary btn-block"
                            data-toggle="modal" data-target="#profile_modal"><b>Změnit obrázek</b>
                    </button>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <!-- /.box -->
    </div>

    <div id="profile_modal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">Změnit profilovou fotografii</h5>
                </div>

                <div class="modal-body">
                    <p>Fotografie by měla mít méně než 1 MB.</p>
                    <form id="profile-picture" action="{{ route('profile.updateProfile') }}"
                          class="dropzone"></form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Zavřít</button>
                    <button type="button" class="btn btn-primary" id="save-profile-picture">Uložit</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script type="text/javascript" src="{{ asset('js/upload.js') }}"></script>
<script type="text/javascript">
    Dropzone.autoDiscover = false;
    $(document).ready(function () {
        var profile = new Dropzone("#profile-picture", {
            maxFilesize: 1,
            maxFiles: 1,
            dictDefaultMessage: 'Sem přesuňte profilový obrázek',
            dictFileTooBig: 'Obrázek je moc velký, maximální velikost je 1 MB',
            init: function () {
                this.on('addedfile', function (file) {
                    if (this.fileTracker) {
                        this.removeFile(this.fileTracker);
                    }
                    this.fileTracker = file;
                });
                this.on('success', function (file) {
                    $(file.previewElement).find('.dz-progress').hide();
                    maturoid.notify('Hotovo', 'Fotografie byla úspěšně změněna. Uvidíte ji po znovunačtení stránky.', 'success')
                });
                this.on('error', function (file) {
                    var el = $(file.previewElement);
                    el.find('.dz-progress').hide();
                    el.hover(function () {
                        el.find('.dz-error-mark').hide();
                    });
                });
            },
            autoProcessQueue: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#save-profile-picture').on('click', function () {
            profile.processQueue();
        });
    });
</script>
@endpush