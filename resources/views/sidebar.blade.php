<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ Auth::user()->getAvatar() }}"
                     class="img-circle" alt="user profile photo"
                     onerror="if (this.src != '100.jpg') this.src = '{{ URL::to('storage/profile') }}/100.jpg';">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::getUser()->name }}</p>
                <small>{{ Auth::getUser()->roles->implode('name', ', ') }}</small>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">Hlavní navigace</li>
            @include('sidebar.item', ['route' => 'home', 'name' => 'Domov', 'icon' => 'tachometer'])

            <li class="header">Maturitní projekt</li>
            @include('sidebar.item', ['route' => 'archiveProject.index', 'name' => 'Archivované projekty', 'icon' => 'archive'])

            @if(Auth::user()->project_id == null)
                @include('sidebar.item', ['route' => 'reserveProject.index', 'name' => 'Projekty k vybrání', 'icon' => 'list'])
            @endif

            @if(Auth::user()->hasRole(['opponent', 'manager', 'admin']) ||
             (Auth::user()->hasRole('student') && Auth::user()->project_id == null &&
             (Auth::user()->opponent_id != null || Auth::user()->manager_id != null)))
                @include('sidebar.item', ['route' => 'makeProject.index', 'name' => 'Vytvořit vlastní', 'icon' => 'plus'])
            @endif

            @if(Auth::user()->hasRole(['student']) && Auth::user()->project_id != null)
                @include('sidebar.item', ['route' => 'project.my', 'name' => 'Můj projekt', 'icon' => 'heart'])
                <li class="{{ $breadcrumbs->last() == 'Úkoly k projektu' ? 'active' : '' }}">
                    <a href="{{ route('project.my', '#tasks') }}">
                        <i class="fa fa-tasks"></i> <span>Úkoly k projektu</span>
                    </a>
                </li>
            @endif

            @if(Auth::user()->hasRole(['opponent', 'manager', 'admin']))
                @include('sidebar.item', ['route' => 'reserveProject.manage', 'name' => 'Správa rezervací projektů', 'icon' => 'inbox'])
                @include('sidebar.item', ['route' => 'manageProject.index', 'name' => 'Správa projektů', 'icon' => 'window-restore'])
            @endif

            @hasrole('admin')
            <li class="header">Admin</li>
            @include('sidebar.item', ['route' => 'users.index', 'name' => 'Uživatelé', 'icon' => 'users'])
            @include('sidebar.item', ['route' => 'categories.index', 'name' => 'Kategorie', 'icon' => 'file-o'])
            @endhasrole
    </section>
    <!-- /.sidebar -->
</aside>