<script src="/vendor/tinymce/tinymce.min.js"></script>
<script>
    tinymce.init({
        selector: 'textarea.editor',
        language: 'cs_CZ',
        browser_spellcheck: true,
        menubar: false,
        {!! isset($height) ? 'height: "' . $height . 'px",' : '' !!}
        plugins: 'preview table code lists link autolink charmap wordcount',
        toolbar1: 'undo redo | preview insert styleselect | bold italic | bullist numlist outdent indent | image table | code',
        style_formats: [{
            title: "Inline",
            items: [{title: "Bold", icon: "bold", format: "bold"}, {
                title: "Italic",
                icon: "italic",
                format: "italic"
            }, {title: "Underline", icon: "underline", format: "underline"}, {
                title: "Strikethrough",
                icon: "strikethrough",
                format: "strikethrough"
            }, {title: "Superscript", icon: "superscript", format: "superscript"}, {
                title: "Subscript",
                icon: "subscript",
                format: "subscript"
            }, {title: "Code", icon: "code", format: "code"}]
        }, {
            title: "Blocks",
            items: [{title: "Paragraph", format: "p"}, {title: "Blockquote", format: "blockquote"}, {
                title: "Div",
                format: "div"
            }, {title: "Pre", format: "pre"}]
        }, {
            title: "Alignment",
            items: [{title: "Left", icon: "alignleft", format: "alignleft"}, {
                title: "Center",
                icon: "aligncenter",
                format: "aligncenter"
            }, {title: "Right", icon: "alignright", format: "alignright"}, {
                title: "Justify",
                icon: "alignjustify",
                format: "alignjustify"
            }]
        }]
    });
</script>