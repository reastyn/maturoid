<section class="content-header">

    <h1>{{ $custom['breadcrumb'] or $breadcrumbs->last() }}</h1>

    <ol class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-cloud-upload"></i>Maturoid</a></li>
        @if($breadcrumbs->count() > 1)
            <li><a href="{{ route($breadcrumbs['url']) }}">{{ $breadcrumbs['name'] }}</a></li>
        @endif
        @if(isset($custom))
            <li class="active"><a
                        href="{{ $custom['href'] or '#' }}">{{ $custom['breadcrumb'] }}</a>
            </li>
        @else
            <li class="active"><a
                        href="{{ Request::route()->getCompiled()->getStaticPrefix() }}">{{ $breadcrumbs->last() }}</a>
            </li>
        @endif
    </ol>

    @yield('header-menu')
</section>