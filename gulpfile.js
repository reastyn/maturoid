var elixir = require('laravel-elixir');

require('laravel-elixir-vueify');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    mix.less(['bootstrap/bootstrap.less', 'theme/AdminLTE.less']);
    mix.webpack('maturoid.js', 'resources/assets/js/plugins');
    // mix.browserify('main.js');
    mix.styles([
            'resources/assets/css/dropzone.css',
            'resources/assets/css/select2.min.css',
            'resources/assets/css/pnotify.css',
            'resources/assets/css/bootstrap-datepicker.css',
            //'resources/assets/css/lightbox.min.css',
            'public/css/app.css',
            'resources/assets/css/datatables.css'],
        'public/css/maturoid.css', './');
    mix.scripts([
        'plugins/jQuery/jquery-2.2.3.min.js',
        'plugins/app.js',
        'plugins/vue/vue.min.js',
        'plugins/bootstrap.min.js',
        'plugins/tables/datatables/datatables.min.js',
        'plugins/select2/select2.js',
        'plugins/pnotify/pnotify.js',
        'plugins/datepicker/bootstrap-datepicker.min.js',
        'plugins/datepicker/locales/bootstrap-datepicker.cs.min.js',
        'plugins/pace/pace.js',
        'plugins/lightbox/lightbox.min.js',
        'plugins/maturoid.js'
    ]);
    mix.scripts('plugins/uploaders/dropzone.min.js', 'public/js/upload.js');
});
