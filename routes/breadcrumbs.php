<?php

Breadcrumbs::register('maturoid', function ($breadcrumbs) {
    $breadcrumbs->push('Maturoid', route('home'));
});

Breadcrumbs::register('admin', function ($breadcrumbs)
{
    $breadcrumbs->parent('maturoid');
    $breadcrumbs->push('Admin', route('users.index'));
});

Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->parent('maturoid');
    $breadcrumbs->push('Domov', route('home'));
});

Breadcrumbs::register('profile.profile.index', function ($breadcrumbs)
{
    $breadcrumbs->parent('maturoid');
    $breadcrumbs->push('Profil', route('profile.profile.index'));
});

Breadcrumbs::register('maturitni-projekty.index', function ($breadcrumbs)
{
    $breadcrumbs->parent('maturoid');
    $breadcrumbs->push('Maturitní projekty', route('maturitni-projekty.index'));
});

Breadcrumbs::register('users.index', function ($breadcrumbs)
{
    $breadcrumbs->parent('admin');
    $breadcrumbs->push('Uživatelé', route('users.index'));
});