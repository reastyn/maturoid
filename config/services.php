<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'google' => [
        'client_id' => '943896270008-6jamf3d20ehmt1i295og66uanuh5jl6a.apps.googleusercontent.com',
        'client_secret' => 'wEGv5N-tmCm_QnkeGo1dy_Oz',
        'redirect' => 'http://localhost/maturoid/public/auth/google/callback',
    ],

    'facebook' => [
        'client_id' => '1736348489971301',
        'client_secret' => '7d13ce16ddd953853376a9490cf78f2f',
        'redirect' => 'http://localhost/maturoid/public/auth/facebook/callback',
    ],

    'twitter' => [
        'client_id' => 'dBvKh86W8Inr2ZuGXfnvLaR2P',
        'client_secret' => '0LJZsgXJqsLrHSLCshKHAxLirkYBTo3VqqtTbfe3Ugkw7vkgUd',
        'redirect' => 'http://localhost/maturoid/public/auth/twitter/callback',
    ]
];
